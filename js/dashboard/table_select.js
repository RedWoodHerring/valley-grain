$(function(){
  var selection = $("#containerdata").children("table").attr('id');
  var results = $("#search").val();
  $("#containerdata").on('click', 'table tbody tr', function(){
      var id = $(this).children("td:first-child").html();
    if(selection === 'tickets'){
      var id = $(this).attr('id');
      var success = function (response) {
          $("#header_text").html('Ticket #'+id);
          $('#container1').html("<h4 class='hidden' id='table'>"+selection+"</h4><h4 id='search_result'>"+results+"</h4>"+"<p class='btn btn-primary back_dashboard'>Back</button>");
          $("#container2").html(response);
          $("#containerdata").html('');
          $("#scripts").html("<script src='/js/dashboard/table_select.js'>");
      };
      data = "ID="+id;
      ajax(data,success,'/php/tickets/dashboard.php');
    }
    if(selection === 'clients'){
      var id = $(this).attr('id');
      var name = $(this).children(2).html();
      var success = function (response) {
        $('#header_text').html("Client: "+name);
          $('#container1').html("<h4 class='hidden' id='table'>"+selection+"</h4><h4 id='search_result'>"+results+"</h4>"+"<p class='btn btn-primary back_dashboard'>Back</button>");
        $('#container2').html(response);
          $("#containerdata").html('');
        $('#info_container').children('h3').attr('id',id);
        var success = function (response) {
            $('#contracts_table').html(response);$("#full_contracts_table").DataTable();};
            data = "selection="+id+"&function=contracts_table&status=0";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#activity_table').html(response);};
        data = "selection="+id+"&function=activity_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#contacts_table').html(response);};
        data = "selection="+id+"&function=contract_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
          $("#scripts").html("<script src='/js/dashboard/table_select.js'>");
        };
      data = "selection="+id+"&function=client_dashboard";
      ajax(data,success,'/php/clients/client_dashboard.php');
    };
    if(selection === 'contracts'){
      var id = $(this).attr('id');
      var name = $(this).children(2).html();
      var success = function (response) {
        $('#header_text').html("Client: "+name);
          $("#containerdata").html('');
          $('#container1').html("<h4 class='hidden' id='table'>"+selection+"</h4><h4 id='search_result'>"+results+"</h4>"+"<p class='btn btn-primary back_dashboard'>Back</button>");
        $('#container2').html(response);
        $('#info_container').children('h3').attr('id',id);
        if($('#status').html() === 'Active'){
            $("#status").css('color', 'green');
        } else {
            $("#status").css('color', 'red');
        }
          $("#scripts").html("<script src='/js/dashboard/table_select.js'>");
      };
      data = 'ID='+id;
      ajax(data,success,'/php/contract/contract_info.php')
    };
    if(selection === 'sale'){
        results = $(this).children("td:first-child").html();
      var id = $(this).attr('id');
      var name = $(this).children(2).html();
      var success = function (response) {
        $('#header_text').html("Client: "+name);
          $("#containerdata").html('');
          $('#container1').html("<h4 class='hidden' id='table'>"+selection+"</h4><h4 id='search_result'>"+results+"</h4>"+"<p class='btn btn-primary back_dashboard'>Back</button>");
        $('#container2').html(response);
          $("#scripts").html("<script src='/js/dashboard/table_select.js'>");
        $('#header_text').html("Bill of Lading #"+id+" Full View");
      };
      data = 'ID='+id;
      ajax(data,success,'/php/sale/bill_of_lading.php')
    };
  });
  $(".back_dashboard").on('click', function(){
      $("#scripts").html("<script src='/js/dashboard/recent.js'>");
      var data = '';
      var success = function(response){
          $('#container1').html("<div class='row'><button class='btn btn-primary' id='create_ticket'>Create Ticket</button><button class='btn btn-primary' id='create_sale'>Create Bill of Lading</button></div>"+response);
      };
      ajax(data,success,'../php/dashboard_search.php');
      $('#header_text').html("Dashboard");
      selection = $("#table").html();
      results = $("#search_result").html();
      console.log(selection+" "+results);
      if (results === ''){
          $('#data').html('');
      } else {
          data = "input="+results+"&table="+selection;
          var if_true = function (response){
              $('#containerdata').html(response);
              //console.log(response);
              $('#search').val(results);
          };
          ajax(data,if_true,'../php/dashboard/search0.php');
          $('#search').val(results);
      }
  });
});
