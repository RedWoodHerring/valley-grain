$(function(){
  //window.history.replaceState({page: "Dashboard"}, "Valley Grain Dashboard", "/dashboard");
  //$("#window_script").html("<script src='/scripts/js/window_change.min.js'></script>");

    $('#search_button').click(function(){
      var input = $('#search_box_container').children().val();
      if (input === ''){
        $('#data').html('');
      } else {
        var table = $("#search_form").serialize();
        var data = "input="+input+"&"+table;
        var if_true = function (response){
          $('#containerdata').html(response);
          //console.log(response);
        };
        ajax(data,if_true,'../php/dashboard/search0.php');
      }
      });

    $("#create_ticket").click(function (){
        $('#my_modal').modal('toggle');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/ticket_management/submit_new.js'></script>");
            $("#modal_title").html('Submit a New Ticket');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_ticket' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=ticket&function=create_new_modal";
        ajax_generation(data, success);
    });
    $("#create_sale").click(function (){
        $('#my_modal').modal('toggle');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/submit_new.js'></script>");
            $("#modal_title").html('Submit a New Bill of Lading');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_sale' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=sale&function=create_new_modal";
        ajax_generation(data, success);
    });
    $(".back").on('click', function(){
      var data = '';
      var success = function(response){
        $('#container1').html(response);
        $('#header_text').html('Dashboard');
      }
      ajax(data,success,'../php/dashboard_search.php');
    });
});
