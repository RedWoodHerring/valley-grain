$(function () {
    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_bin_id").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=bin_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Bin Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Bin Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_sale_id").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=sale_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Bill of Lading Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Bill of Lading Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_product_id").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=product_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Product Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Product Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_fat").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=fat&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Fat Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Fat Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_fiber").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=fiber&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Fiber Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Fiber Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_ash").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=ash&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Ash Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Ash Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_aflatoxin").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=aflatoxin&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Aflatoxin Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Aflatoxin Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_protein").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=protein&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Protein Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Protein Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_moisture").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=moisture&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Moisture Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Moisture Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_test_weight").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=test_weight&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Test Weight Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Test Weight Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
    $(".submit_vomitoxin").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=analysis&column=vomitoxin&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Vomitoxin Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Vomitoxin Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
    });
});