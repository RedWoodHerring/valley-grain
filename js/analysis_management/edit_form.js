$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().attr('id');
        options_form(id,value,'/js/analysis_management/change.js',submit,'Analysis Options',button_container);
    });
    $('.sale').click(function () {
        var analysis = $(this).attr('id');
        var sale_id = document.getElementById('sale_id_value').innerHTML;
        success = function (response){$('#modal_body').html(response)};
        $('#modal_footer').html("<button class='btn btn-primary back' id='"+sale_id+">Back</button>");
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_body").html("<h5 class='text-center center-block'>Bill of Lading #" + sale_id + "</h5>" + response);
            $('#modal_footer').html("<button id='"+analysis+"' class='btn btn-info back'>Back</button>");
        };
        data = "id="+sale_id+"&function=sale_options&selection=sale_relation";
        ajax_generation(data, success);
    });
    $('.back').click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
});