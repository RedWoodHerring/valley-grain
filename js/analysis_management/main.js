$(function(){
    $('#table_id').on('click', 'tbody tr', function() {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_analysis');
            $('#my_modal').modal('toggle');
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
        $('#modal_footer').html("<button id='"+id+"' class='btn btn-info sale'>Bill of Lading</button>");
    });
    $(".view_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_title").html('Ticket #'+id+' Measurements');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('ticket_measurements',success,id);
    });
});
