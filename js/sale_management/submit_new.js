$(function () {
    var sale_button = $('#sale');
    $('#submit_new_sale').click(function (){
        err = $('#modal_msg');
        body = $('#modal_body');
        data = $("#bill_of_lading").serialize()+'&function=create&selection=sale&table=sale';
        func_yes = function () {
            body.html("<p class='alert bg-success'>Bill of Lading Added</p><button id='new_sale_button' class='btn btn-default justify-center'>Create New Ticket</button>");
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
        };
        func_no = function (response) {
            err.html("<p>Bill of Lading cannot be added, please contact an admin</p>");
            console.log(response);
        };
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
    });
    $('.back').click(function(){
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_sale_button' class='btn btn-default justify-center'>Create Bill of Lading</button>");
        $('#header_text').html("Bill of Lading");
        $(".nav li").removeClass("selected");
        sale_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
});