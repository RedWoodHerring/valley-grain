$(function(){
  $(".back").click(function(){
    selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
    data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
    ajax_generation(data,data_table_reload);
    $("#container1").html("<button id='new_sale_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Bill of Lading</button>");
    $('#header_text').html("Bill of Lading");
    $(".nav li").removeClass("selected");
    $('#admin_sub_menu').removeClass('show');
  });
});
