$(function() {
    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });

    $(".submit_date_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=date&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_PO_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=PO&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_deliver_no_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=deliver_no&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_pounds_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=pounds&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_bushels_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=bushels&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_client_id_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=client_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_seal_number_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=seal_number&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_deliver_no_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=deliver_no&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_carrier_id_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=carrier_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_trailer_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=trailer&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_truck_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=truck&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_product_id_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=product_id&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_f_paid_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=f_paid&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $(".submit_f_rate_value").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=sale&column=f_rate&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
});