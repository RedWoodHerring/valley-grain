$(function(){
    $("#new_sale_button").click(function () {
        success = function (response) {
            $("#scripts").html("<script src='/js/sale_management/submit_new.js'></script>");
            $("#header_text").html('Submit a New Bill of Lading');
            $("#container2").html(response);
            $("#container1").html("<button id='submit_new_sale' class='btn btn-primary'>Submit</button><button class='btn btn-default justify-center back'>Back</button>");
        };
        data = "selection=sale&function=create_new_modal";
        ajax_generation(data, success);
    });
    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/sale_management/edit_form.js'></script>");
            $("#modal_title").html('Sale #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_sale');
            $('#modal_footer').html("<button id='"+id+"' class='btn btn-info analysis'>Analysis</button>");
        };
        data = "id="+id+"&function=sale_options&selection=sale";
        ajax_generation(data, success);
    });
    $('#table_id').on('click', 'tbody tr', function() {
        var id = $(this).attr('id');
        data = 'ID='+id;
        success = function (response){
            $('#container1').html("<div class='center-block'><button class='btn btn-info back'>Back</button></div>")
            $('#container2').html(response);
            $("#scripts").html("<script src='/js/sale_management/dashboard.js'></script>")
        };
        $.ajax({
            url: "../php/sale/bill_of_lading.php",
            data: data,
            type: 'post',
            async: true,
            context: document.body,
            success: function(response) {success(response);}
        });
        $('#header_text').html("Bill of Lading #"+id+" Full View");
    });
});
