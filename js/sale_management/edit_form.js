$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().prev().attr('id');
        options_form(id,value,'/js/sale_management/change.js',submit,'Sale Options',button_container);
    });
    $('.analysis').click(function () {
        var sale_id = $(this).attr('id');
        success = function (response){$('#modal_body').html(response)};
        $('#modal_footer').html("<button class='btn btn-primary back' id='"+sale_id+">Back</button>");
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            if ($('#modal_title').hasClass('from_sale')) {
                $("#modal_body").html("<h5 class='text-center center-block'>Analysis from Bill of Lading #" + sale_id + "</h5>" + response);
                $('#modal_footer').html("<button id='"+sale_id+"' class='btn btn-info back'>Back</button>");
            } else {
                $("#modal_body").html(response);
            }
        };
        data = "id="+sale_id+"&function=analysis_options&selection=analysis_relation";
        ajax_generation(data, success);
    });
    $('.back').click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/analysis_management/edit_form.js'></script>");
            $("#modal_title").html('Analysis #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_title").addClass('from_analysis');
        };
        data = "id="+id+"&function=analysis_options&selection=analysis";
        ajax_generation(data, success);
        $('#modal_footer').html("<button id='"+id+"' class='btn btn-info sale'>Bill of Lading</button>");
    });
    $(".delete").click(function (){
        id =  $(this).attr('id');
        data = 'selection='+id+'&function=delete_sale';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>Bill of Lading Deleted</p>")};
        func_no = function (){$('#err').html("<p class='alert bg-danger'>Bill of Lading cannot be deleted, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        erase_modal();
        $('#my_modal').modal('toggle');
        $('#err').fadeOut(5000);
    });
});