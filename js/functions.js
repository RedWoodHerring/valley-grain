//TODO write a function for each type of ajax call to simplify the code
var login = function () {
    var err = $('#err');
    var username = $("#username").val().trim();
    var password = $("#password").val().trim();
    if (!password && !username) {
        err.html("<p class='alert bg-danger'>Please Enter Your Credentials</p>");
    } else
    if (!username) {
        err.html("<p class='alert bg-danger'>Please Enter a Email Address</p>");
    } else
    if (!password) {
        err.html("<p class='alert bg-danger'>Please Enter a Password</p>");
    } else {
        data = $("#login-form").serialize() + '&function=login';
        func_no = function () {$('#err').html("failed");};
        func_yes = function () {window.location.href = 'portal/';};
        ajax_send_data(data, func_yes, func_no);
        return;
    }
};

var ajax_send_data = function (data,func_yes,func_no){
    $.ajax({
        type: "post",
        url: '../php/handler.php',
        data: data,
        async: true,
        success: function (response) {
            if (response === 'true' || response === 'truetrue'){func_yes();} else {func_no(response);}}
    });
};

var ajax_generation = function(data,success){
    $.ajax({
        url: "../php/handler.php",
        data: data,
        type: 'post',
        async: true,
        success: function(response) {success(response);}
    });
};

var ajax = function(data,success,url){
    $.ajax({
        url: url,
        data: data,
        type: 'post',
        async: true,
        success: function(response) {success(response);}
    });
};

var ajax_selector_generation = function(func,success,id){
    $.ajax({
        url: "../php/handler.php",
        data: 'function='+func+'&selection='+id,
        type: 'post',
        async: true,
        context: document.body,
        success: function(response) {success(response);}
    });
};

var selected_option = function (value) {
    $('#change > option').each(function (){
        if ($(this).attr('value') === value){
            this.setAttribute('selected','selected');
        }
    });
};

var data_table_reload = function (response){$("#container2").html(response);$("#table_id").DataTable();};

var options_form = function (id,value,script,submit,title,button_container){
    var content = document.getElementById(value).innerHTML;
    if (submit === 'submit_Level') {
        $("#modal_scripts").html("<script src="+script+"></script>");
        var success = function (response){ return response;};
        var data = 'function=select_generation&selection=level';
        console.log(ajax_generation(data, success));
        button_container.html("<button id='"+id+"' class='btn btn-sm btn-primary pull-right "+submit+"'>Submit</button><button id='"+id+"' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_bin_id' || submit === 'submit_bin_value') {
        $("#modal_scripts").html("<script src="+script+"></script>");
        success = function (response){$("#"+value).html(response)};
        data = 'function=select_generation&selection=bin&value='+value;
        console.log(ajax_generation(data, success));
        button_container.html("<button id='"+id+"' class='btn btn-sm btn-primary pull-right "+submit+"'>Submit</button><button id='"+id+"' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_product_id' || submit === 'submit_product_id_value' || submit === 'submit_product_value') {
        $("#modal_scripts").html("<script src="+script+"></script>");
        success = function (response){$("#"+value).html(response)};
        data = 'function=select_generation&selection=product&value='+value;
        console.log(ajax_generation(data, success));
        button_container.html("<button id='"+id+"' class='btn btn-sm btn-primary pull-right "+submit+"'>Submit</button><button id='"+id+"' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_date_value' || submit === 'submit_date') {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        $("#" + value).html("<input type='date' value='" + content + "' id='change'>");
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_client_id_value' || submit === 'submit_client_value') {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        success = function (response){$("#"+value).html(response)};
        data = 'function=select_generation&selection=client&value='+value;
        console.log(ajax_generation(data, success));
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_carrier_id_value') {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        success = function (response){$("#"+value).html(response)};
        data = 'function=select_generation&selection=carrier&value='+value;
        console.log(ajax_generation(data, success));
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_type_value') {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        success = function (response){$("#"+value).html(response)};
        data = 'function=select_generation&selection=type&value='+value;
        console.log(ajax_generation(data, success));
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    } else if (submit === 'submit_password') {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        $("#" + value).html("<input type='password' id='change'>");
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide()
    } else {
        $("#modal_scripts").html("<script src=" + script + "></script>");
        $("#" + value).html("<input value='" + content + "' id='change'>");
        button_container.html("<button id='" + id + "' class='btn btn-sm btn-primary pull-right " + submit + "'>Submit</button><button id='" + id + "' class='btn btn-sm btn-primary pull-right options_button'>Cancel</button>");
        $("#modal_title").html(title);
        $('.edit').hide();
    }
};

var change_option = function (id,column,title,table) {
    var input = $('#change');
    input_submit = input.val();
    var data = "function=update_table&selection="+id+"&table="+table+"&column="+column+"&change="+input_submit;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>"+title+" Changed</p>")};
    var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>"+title+" Unable To Be Changed, please contact an admin</p>")};
    ajax_send_data(data,func_yes,func_no);
    ajax_generation('client_table',data_table_reload);

};

//      MODAL

//Erase any components added to modal

var erase_modal = function () {
    $("#modal_title").html('');
    $("#modal_body").html('');
    $("#modal_footer").html('');
    $("#modal_msg").html('');
    $('#modal_msg').html('');
};

var reset_client_modal = function () {
    $("#modal_footer").html("<button onclick='erase_modal()' class='close' data-dismiss='modal' class='btn btn-primary'>Close</button>");
};

//      TICKETS

var submit_ticket_item = function (id,func){
    var data = 'change='+$("#new_ticket_item").val()+'&function='+func+'&selection='+id;
    var func_no = $('#modal_msg').html("<p class='alert bg-danger'>Unable to Change Item, Please Contact an Administrator</p>");
    var msg = $('#modal_msg').html("<p class='alert bg-success'>Items Has Been Changed</p>");
    var table_refresh = ajax_generation('ticket_table');
    var func_yes = msg+table_refresh;
    ajax_send_data(data,func_yes,func_no);
    ticket_options_modal(id);
};

var view_file = function (id){
    data = 'ID='+id;
    var newWindow=window.open(ajax(data,success,'../php/contract/pdf_pull.php'),'height=200,width=150');
    newWindow.focus();
    //newWindow.print();
    //newWindow.close();
};

var chart = function (id,data_points,heading){
    var ctx = document.getElementById("pie_chart").getContext('2d');
    console.log(data_points[1]);
    data = {
        datasets: [{
            data: data_points,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: heading
    };

    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: {}
    });
    
};