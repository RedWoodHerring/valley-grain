$(function() {
    $("#new_user_button").click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/submit_user_handler.js'></script>");
            $("#modal_title").html('Submit a New User');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_user' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=user&function=create_new_modal";
        ajax_generation(data, success);
    });
    $("tbody tr").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
          $('#my_modal').modal('toggle');
          $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
          $("#modal_title").html('User #'+id+' Options');
          $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
});
