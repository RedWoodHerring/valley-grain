$(function () {
    $("#submit_new_user").click(function () {
        err= $('#err_form');
        var username = $("#email").val().trim();
        var password = $("#password").val().trim();
        var first_name = $("#f_name").val().trim();
        var last_name = $("#l_name").val().trim();
        if (!password || !username || !first_name || !last_name || !password) {
            err.html("<p class='alert bg-danger'>Please Fill the Entire Form");
        } else {
            data = $("#new_user_form").serialize()+'&function=create&selection=user&table=login';
            func_yes = function () {
                $('#modal_msg').html("<p class='alert bg-success'>New User "+username+" Added</p>").fadeIn(1200).fadeOut(1200);
                $('#modal_body').html("<button id='new_user_button' class='btn btn-default justify-center'>Create New User</button>");
            };
            func_no = function () {err.html("<p class='alert bg-danger'>User cannot be added, please contact an admin</p>").fadeIn(1200).fadeOut(1200);};
            ajax_send_data(data,func_yes,func_no);
            data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
            ajax_generation(data,data_table_reload);
            $('#modal_footer').html('');
        }
    })
});
