$(function () {
    $(".delete_user").click(function (){
        id =  $(this).attr('id');
        data = 'selection='+id+'&function=delete_user';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>User Deleted</p>")};
        func_no = function (){$('#err').html("<p class='alert bg-danger'>User cannot be deleted, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        success = function (response) {$("#container2").html(response);$("#table_id").DataTable();};
        ajax_generation('user_table',success);
        erase_modal();
        $('#my_modal').modal('toggle');
        $('#err').fadeOut(5000);
    });
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().attr('id');
        options_form(id,value,'/js/user_management/user_change_handler.js',submit,'User Options',button_container);
    });
});