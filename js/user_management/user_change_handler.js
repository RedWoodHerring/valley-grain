$(function () {

    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
    $(".submit_password").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=login&column=password&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
    $(".submit_Level").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=login&column=Level&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
    $(".submit_email").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=login&column=email&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
    $(".submit_f_name").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=login&column=f_name&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
    $(".submit_l_name").click(function () {
        var id = $(this).attr('id');
        var input = $('#change');
        var input_submit = input.val();
        var data = 'function=update_table&selection='+id+'&table=login&column=l_name&change='+input_submit;
        var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Email Changed</p>")};
        var func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Email Unable To Be Changed, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/user_management/user_edit_form_handler.js'></script>");
            $("#modal_title").html('User #'+id+' Options');
            $("#modal_body").html(response);
        };
        data = "id="+id+"&function=options_modal&selection=user";
        ajax_generation(data, success);
    });
});