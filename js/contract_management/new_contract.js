$(function (){
    $('#packaging_select').change(function (){
        if($(this).val() === 'other'){
            $('#packaging_other').removeClass('hidden');
            $('#packaging_other').addClass('visible');
        }
    });
    $('#delivery_select').change(function (){
        if($(this).val() === 'other'){
            $('#delivery_other').removeClass('hidden');
            $('#delivery_other').addClass('visible');
        }
    });
    $('#new_client_button').click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/submit_client_handler.js'></script>");
            $("#modal_title").html('Contract Form > Submit a New Client');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_client' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=client&function=create_new_modal";
        ajax_generation(data, success);
    });
    $('#new_product_button').click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/submit_client_handler.js'></script>");
            $("#modal_title").html('Contract Form > Submit a New Client');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_client' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=client&function=create_new_modal";
        ajax_generation(data, success);
    });
    $('#save').click(function () {
        var data = $('#new_contract_form').serialize();
        function success(response){
            if (response === 'true'){
                $('#err').html("<p class='alert bg-success'>Contract Added</p>").fadeIn(2400).fadeOut(2400);
                var newWindow=window.open('../php/contract/pdf_pull.php','height=200,width=150');
                newWindow.focus();
                newWindow.print();

                document.getElementById('new_contract_form').reset();
                $("input").val('');
                $("select").val('');
            } else {
                $('#err').html("<p class='alert bg-danger'>Contract Failed To add</p>");
                console.log(response);
            }
        }
        ajax(data,success,'/php/contract/submit_new_contract.php');
        //var newWindow=window.open('../php/contract/pdf_pull.php','height=200,width=150');
        //newWindow.focus();
        //newWindow.print();
        //newWindow.close();
    });
    $(".back").click(function(){
      data = '';
      success = function (response){
          $('#scripts').html("<script src='/js/contract_management/main.js'></script>");
          $('#container1').html("<button class='btn btn-primary' id='new_contract'>New Contract</button>");
          $('#container2').html(response);
          $("#container2 table").DataTable();
      };
      ajax(data,success,'../php/contract/contract_list.php');
      $("#container1").html("<button id='new_contract_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Contract</button>");
      $('#header_text').html("Contract");
      $(".nav li").removeClass("selected");
      $("#contracts").addClass('selected');
      $('#admin_sub_menu').removeClass('show');
    });
    $(".back-ticket").click(function(){
        selection = 'ID,date,product_id,client_id,dockage_price,bin_id,net_bushels';
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_ticket_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Ticket</button>");
        $('#header_text').html("Tickets");
        $(".nav li").removeClass("selected");
        tickets_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/ticket_management/submit_new.js'></script>");
            $("#modal_title").html('Submit a New Ticket');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_ticket' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=ticket&function=create_new_modal";
        ajax_generation(data, success);
    });
});
