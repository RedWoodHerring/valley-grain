$(function (){
    var id = $("#selection").html();
    var contract = $("#selection").html();
    var total_qty = parseInt($("#total_qty").html());
    var current_qty = parseInt($("#current_qty").html());
    var remainder = total_qty - current_qty;
    data = [remainder, current_qty];
    headings = ['Product Waiting Delivery','Product Received'];
    chart(id,data,headings);

    $("#tickets_table tbody tr td").click(function (){
        var ticket_id = $(this).parent().children('td:first-child').html();
        success = function (response) {
            $("#header_text").html('Ticket #'+ticket_id);
            $("#container1").html("<button id='"+id+"' class='btn btn-info back_contract text-center center-block'>Back to Contract #"+id+"</button>");
            $("#container2").html(response);
            $("#scripts").html("<script src='/js/ticket_management/dashboard.js'></script>");
        };
        data = "ID="+id;
        ajax(data,success,'/php/tickets/dashboard.php');
    });
    $(".view_contract").click(function (){
        id = $(this).attr('id');
        var newWindow=window.open('../php/pdf_pull.php?ID='+id,'height=200,width=150');
        newWindow.focus();
      });
    $(".back").click(function(){
      data = '';
      success = function (response){
          $('#scripts').html("<script src='/js/contract_management/main.js'></script>");
          $('#container1').html("<button class='btn btn-primary' id='new_contract'>New Contract</button>");
          $('#container2').html(response);
          $("#container2 table").DataTable();
      };
      ajax(data,success,'../php/contract/contract_list.php');
      $("#container1").html("<button id='new_contract_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Contract</button>");
      $('#header_text').html("Contract");
      $(".nav li").removeClass("selected");
      $("#contracts").addClass('selected');
      $('#admin_sub_menu').removeClass('show');
    });
    $(".back_client").click(function(){
      var id = $(this).attr('id');
      var name = $(".client_button").html();
      $('#styles').attr('href','/css/client_dashboard.css');
      $('#header_text').html("Client: "+name);
      $('#container1').html("<button class='btn btn-primary back center-block'>Back to Clients</button>");
      var success = function (response) {
        $('#container2').html(response);
        $('#info_container').children('h3').attr('id',id);
        var success = function (response) {
            $('#contracts_table').html(response);$("#full_contracts_table").DataTable();};
        data = "selection="+id+"&function=contracts_table&status=0";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#activity_table').html(response);};
        data = "selection="+id+"&function=activity_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#contacts_table').html(response);};
        data = "selection="+id+"&function=contract_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        $('#scripts').html("<script src='/js/client_management/client_dashboard.js'></script>");
      };
      data = "selection="+id+"&function=client_dashboard";
      ajax(data,success,'/php/clients/client_dashboard.php');
    });
    $(".client_button").click(function (){
      var id = $(this).attr('id');
      var name = $(this).html();
      $('#styles').attr('href','/css/client_dashboard.css');
      $('#header_text').html("Client:"+name);
      $('#container1').html("<button id='"+contract+"' class='btn btn-primary back_contract center-block'>Back to Contract #"+contract+"</button>");
      var success = function (response) {
        $('#container2').html(response);
        $('#info_container').children('h3').attr('id',id);
        var success = function (response) {
            $('#contracts_table').html(response);$("#full_contracts_table").DataTable();};
        data = "selection="+id+"&function=contracts_table&status=0";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#activity_table').html(response);};
        data = "selection="+id+"&function=activity_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#contacts_table').html(response);};
        data = "selection="+id+"&function=contract_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        $('#scripts').html("<script src='/js/client_management/client_dashboard.js'></script>");
      };
      data = "selection="+id+"&function=client_dashboard";
      ajax(data,success,'/php/clients/client_dashboard.php');
    });
});
