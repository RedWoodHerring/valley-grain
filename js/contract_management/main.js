$(function (){
    $("#new_contract").click(function (){
        data = '';
        success = function (response){
            $('#container2').html(response+"<script src='/js/contract_management/new_contract.js'></script>");
            $('#container1').html("<div class='row justify-center'><button class='btn btn-primary back'>Back</button><button id='save' class='btn btn-primary'>Save</button></div>");
        };
        ajax(data,success,'../php/contract/new_contract_form.php');
        $('#header_text').html("New Contract");
    });
    $("#new_contract_ticket").click(function (){
        data = '';
        success = function (response){
            $('#container2').html(response+"<script src='/js/contract_management/new_contract.js'></script>");
            $('#container1').html("<div class='row justify-center'><button class='btn btn-primary back-ticket'>Back</button><button id='save' class='btn btn-primary'>Save</button></div>");
        };
        ajax(data,success,'../php/contract/new_contract_form.php');
        $('#header_text').html("New Contract");
    });
    $('#contracts_table').on('click', 'tbody tr', function() {
        var id = $(this).children('td:first-child').html();
        data = 'ID='+id;
        success = function (response){
            $('#container2').html(response);
            $('#container1').html("<button class='btn btn-primary back'>Back</button><button id='"+id+"' class='btn btn-info view_contract'>View Contract</button>");
            $("#scripts").append("<script src='/js/contract_management/individual_contract.js'></script>");
            if($('#status').html() === 'Active'){
                $("#status").css('color', 'green');
            } else {
                $("#status").css('color', 'red');
            }
        };
        ajax(data,success,'/php/contract/contract_info.php');
        $('#header_text').html("Contract # <span id='selection'>"+id+"</span>");
    })
});
