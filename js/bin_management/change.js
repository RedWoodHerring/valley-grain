$(function(){
  $('.submit_location').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=bins&test=simple_test&column=location&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Location Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){$('#modal_msg').html("<p class='alert bg-warning'>Location Taken, Please Try Again</p>").fadeIn(1200).fadeOut(1200)};
    ajax_send_data(data,func_yes,func_no);
    selection = 'ID,product_name';
    data = 'selection='+selection+'&function=main_table&table=bins&title=Create New Bin&script=/js/bin_management/main.js&id=new_bin_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/bin_management/edit_form.js'></script>");
        $("#modal_title").html('Bin #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=bin_options&selection=Bins";
    ajax_generation(data, success);
    var selection = 'ID,location';
    data = 'selection='+selection+'&function=main_table&table=bins&title=Create New Bin&script=/js/bin_management/main.js&id=new_bin_button';
    ajax_generation(data,data_table_reload);
  });
});
