$(function (){
    $('#submit_new_bin').click(function (){
        var data = $('#new_bin_form').serialize()+'&function=create&selection=bin&table=bins';
        func_yes = function (response) {

            if(response === 'already created'){
              $('#modal_msg').html("<p class='alert bg-warning text-center'>Bin Already Created</p>").fadeIn(1200).fadeOut(1200);
              $('#modal_footer').html('');
            } else {
              $('#modal_msg').html("<p class='alert bg-success text-center'>Bin Added</p>").fadeIn(1200).fadeOut(1200);
              $('#modal_body').html("<button id='new_bin_button' class='btn btn-default justify-center'>Create New Bin</button>");
              $('#modal_footer').html('');
            }
        };
        func_no = function (response) {
          $('#modal_msg').html("<p class='alert bg-warning text-center'>Bint Already Created</p>").fadeIn(1200).fadeOut(1200);
          $('#modal_footer').html('');
        };
        ajax_send_data(data,func_yes,func_no);
        var selection = 'ID,location';
        data = 'selection='+selection+'&function=main_table&table=bins&title=Create New Bin&script=/js/bin_management/main.js&id=new_bin_button';
        ajax_generation(data,data_table_reload);
    })
});
