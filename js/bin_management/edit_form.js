$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().attr('id');
        options_form(id,value,'/js/bin_management/change.js',submit,'Bin Options',button_container);
    });
    $(".delete").click(function (){
        var id = $(this).attr('id');
        data = 'selection='+id+'&function=delete&table=bins';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>Bin Deleted</p>").fadeIn(900).fadeOut(900)};
        func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Bin cannot be deleted, please contact an admin</p>").fadeIn(900).fadeOut(900)};
        ajax_send_data(data,func_yes,func_no);
        selection = '*';
        data = 'selection='+selection+'&function=main_table&table=bins&title=Create New Bin&script=/js/bin_management/main.js&id=new_bin_button';
        ajax_generation(data,data_table_reload);
        erase_modal();
        $('#my_modal').modal('toggle');
    })
});
