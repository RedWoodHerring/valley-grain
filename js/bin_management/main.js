$(function(){
  $('#table_id').on('click', 'tbody tr', function() {
      var id = $(this).attr('id');
      success = function (response) {
          $("#modal_scripts").html("<script src='/js/bin_management/edit_form.js'></script>");
          $("#modal_title").html('Bin #'+id+' Options');
          $("#modal_body").html(response);
          $('#my_modal').modal('toggle');
      };
      data = "id="+id+"&function=bin_options&selection=Bins";
      ajax_generation(data, success);
  });
  $("#new_bin_button").click(function () {
      success = function (response) {
          $("#modal_scripts").html("<script src='/js/bin_management/submit_new.js'></script>");
          $("#modal_title").html('Create a New Bin');
          $("#modal_body").html(response);
          $("#modal_footer").html("<button id='submit_new_bin' class='btn btn-primary'>Submit</button>");
      };
      data = "selection=bin&function=create_new_modal";
      ajax_generation(data, success);
  });
});
