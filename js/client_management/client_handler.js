$(function() {
    $("#new_client_button").click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/submit_client_handler.js'></script>");
            $("#modal_title").html('Submit a New Client');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_client' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=client&function=create_new_modal";
        ajax_generation(data, success);
    });
    $("tbody tr").click(function(){
        var id = $(this).attr('id');
        var name = $(this).children("td:nth-child(2)").html();
        $('#styles').attr('href','/css/client_dashboard.css');
        $('#header_text').html("Client: "+name);
        $('#container1').html("<button class='btn btn-primary back center-block'>Back to Clients</button>");
        var success = function (response) {
          $('#container2').html(response);
          $('#info_container').children('h3').attr('id',id);
          var success = function (response) {
              $('#contracts_table').html(response);$("#full_contracts_table").DataTable();};
          data = "selection="+id+"&function=contracts_table&status=0";
          ajax(data,success,'../php/clients/client_dashboard.php');
          success = function (response) {
              $('#activity_table').html(response);};
          data = "selection="+id+"&function=activity_table_initial";
          ajax(data,success,'../php/clients/client_dashboard.php');
          success = function (response) {
              $('#contacts_table').html(response);};
          data = "selection="+id+"&function=contract_table_initial";
          ajax(data,success,'../php/clients/client_dashboard.php');
          $('#scripts').html("<script src='/js/client_management/client_dashboard.js'></script>");
        };
        data = "selection="+id+"&function=client_dashboard";
        ajax(data,success,'/php/clients/client_dashboard.php');

    });
});

///var id = $(this).attr('id');
//var success = function (response) {
    //$("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
    //$("#modal_title").html('Client #'+id+' Options');
    //$("#modal_body").html(response);
//};
//ajax_selector_generation('edit_client_modal',success,id);
