$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().prev().attr('id');
        options_form(id,value,'/js/client_management/client_change_handler.js',submit,'Client Options - Name',button_container);
    });
    $(".edit_address").click(function () {
        var id = $(this).attr('id');
        options_form(id,'address_value','/js/client_management/client_change_handler.js','submit_address','Client Options - Address');
    });
    $(".edit_city").click(function () {
        var id = $(this).attr('id');
        options_form(id,'city_value','/js/client_management/client_change_handler.js','submit_city','Client Options - City');
    });
    $(".edit_state").click(function () {
        var id = $(this).attr('id');
        var value = document.getElementById('state_value').innerHTML;
        var func = 'client_state_form';
        var success = function (response) {
            $("#state_container").html("<select id='change'>"+response+"</select>");
            selected_option(value);
        };
        ajax_generation(func,success);
        $("#modal_scripts").html("<script src='/js/client_management/client_change_handler.js'></script>");
        $("#modal_footer").html("<button id='"+id+"' class='btn btn-primary submit_state'>Submit</button><button id='"+id+"' class='btn btn-primary options_button'>Cancel</button>");
        $("#modal_title").html('Client Options - State');
        $('.edit_button').hide();

    });
    $(".edit_zip").click(function () {
        var id = $(this).attr('id');
        options_form(id,'zip_value','/js/client_management/client_change_handler.js','zip_name','Client Options - ZIP');
    });
    $(".edit_phone").click(function () {
        var id = $(this).attr('id');
        options_form(id,'phone_value','/js/client_management/client_change_handler.js','phone_name','Client Options - Phone');
    });

    $(".delete_client").click(function (){
        id =  $(this).attr('id');
        data = 'selection='+id+'&function=delete_client';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>Client Deleted</p>")};
        func_no = function (){$('#err').html("<p class='alert bg-danger'>Client cannot be deleted, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        ajax_generation('client_table',data_table_reload);
        erase_modal();
        $('#my_modal').modal('toggle');
        $('#err').fadeOut(5000);
    });
});