$(function (){
  $('.submit').click(function (){
    var id = $(this).attr('id');
    var data = $("#new_contact").serialize()+"&ID="+id;
    var success = function (response){
      if(response === 'true'){
        $('#modal_msg').html("<p class='alert bg-success'>Contact Created</p>");
      } else {
        $('#modal_msg').html("<p class='alert bg-warning'>Contact Unable to be Created</p>");
      }
    }
    ajax(data,success, '/php/clients/create_contact.php');
  });
});
