$(function () {
  var i = 0;
    $("#submit_new_client").click(function () {
        var name = $("#Name").val().trim();
        var s_address = $("#s_address").val().trim();
        var city = $("#city").val().trim();
        var state = $("#state").val().trim();
        var zip = $("#zip").val().trim();
        var phone = $("#phone").val().trim();
        if (!name || !s_address || !city || !state || !zip || !phone) {
            $('#err_form').html("<p class='alert bg-danger'>Please Fill the Entire Form").fadeIn(1200).fadeOut(1200);
        } else {
            data = $("#new_client_form").serialize()+'&function=create&selection=client&table=clients';
            func_yes = function () {
                $('#modal_msg').html("<p class='alert bg-success'>New Client "+name+" Added</p>");
                $('#modal_body').html("<button id='new_client_button' class='btn btn-default justify-center'>Create New Client</button>").fadeIn(1200).fadeOut(1200);
                $('#modal_footer').html('');
            };
            func_no = function () {console.log(response);$('#err_form').html("<p class='alert bg-danger'>Client cannot be added, please contact an admin</p>");};
            var title = $("#modal_title").html();
            ajax_send_data(data, func_yes, func_no);
            data = '';
            for(k = 0; k <= i; k++){
              var id = "contact"+k;
              if ($("#"+id).children("div:nth-child(2)").children('input').val() !== 'undefined'){
                data = $("#"+id).serialize()+"&client_name="+name+"&function=create&selection=contacts&table=contacts";
                func_yes = function () {};
                func_no = function () {console.log(response);};
                ajax_send_data(data, func_yes, func_no);
              }
            }
            if (title === 'Contract Form &gt; Submit a New Client'){
                success = function (response){$('#client_select').html(response).fadeIn(1200).fadeOut(1200);};
                ajax_generation('selection=client&function=select_generation',success);
            } else {
              data = "selection=ID,name,phone,type&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
              ajax_generation(data,data_table_reload);
            }
        }
    });

    $("#new_contact").click(function(){
        var success = function(response){$("#contact_container").append("<form id='contact"+i+"' class='form-group text-center'><h7 class='"+i+"'>Contact "+i+"</h7>"+response+"</form>");};
        var data = "function=contact_form";
        ajax(data,success,'../php/clients/new_client.php');
        i++;
    });
});
