$(function(){
  $('.edit').click(function (){
    var selection = $(this).parent().prev().prev().html();
    var id = $(this).parent().attr('id');
    var input = $(this).parent().prev('p');
    var container = $(this).parent();
    input.html("<input value='"+input.html()+"' name='"+selection+"'/>");
    container.html("<button class='btn btn-sm cancel'>cancel</button><button class='btn btn-sm submit'>Submit</button>");
    $('#modal_scripts').html("<script src='/js/client_management/change.js'></script>");
  });
  $(".make_primary").click(function(){
    var primary_id = $("#primary_contact").children('div').attr('id');
    var id = $(this).attr('id');
    var data = "ID="+id+"&primary_id="+primary_id+"&function=set_primary";

    var success = function(response){
      var id = response;
      console.log(response);
      $('#modal_msg').html("<p class='alert bg-success'>Primary Changed</p>");
      success = function (response) {
        $("#modal_scripts").html("<script src='/js/client_management/edit_contacts.js'></script>");
        $("#modal_title").html('Client #'+id+' Contacts');
        $("#modal_body").html(response);
      };
      data = "ID="+id;
      ajax(data,success,'/php/clients/all_contacts.php');
    }
    console.log(data);
    ajax(data,success,'/php/clients/change.php');
  });
});
