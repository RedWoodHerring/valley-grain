$(function (){
    var id = $("#info_container").children('h3').attr('id');
    $(".back").click(function(){
      data = "selection=ID,name,phone,type&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
      ajax_generation(data,data_table_reload);
      $("#container1").html("<button id='new_client_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Client</button>");
      $('#header_text').html("Clients");
      $(".nav li").removeClass("selected");
      $('#admin_sub_menu').removeClass('show');
      $('#clients').addClass('selected');
    });
    $('#activity_table').on('click', 'table tbody tr', function() {
      var id = $("#info_container").children('h3').attr('id');
      var ticket_id = $(this).children('td:first-child').html();
      success = function (response) {
          $("#header_text").html('Ticket #'+ticket_id);
          $("#container1").html("<button id='"+id+"' class='btn btn-info back_client text-center center-block'>Back to Client #"+id+"</button>");
          $("#container2").html(response);
          $("#scripts").html("<script src='/js/ticket_management/dashboard.js'></script>");
      };
      data = "ID="+id;
      ajax(data,success,'/php/tickets/dashboard.php');
    });
    $('.back_contract').click(function (){
        var id = $(this).attr('id');
        data = 'ID='+id;
        success = function (response){
            $('#container2').html(response);
            $('#container1').html("<button class='btn btn-primary back'>Back</button><button id='"+id+"' class='btn btn-info view_contract'>View Contract</button>");
            $("#scripts").append("<script src='/js/contract_management/individual_contract.js'></script>");
            if($('#status').html() === 'Active'){
                $("#status").css('color', 'green');
            } else {
                $("#status").css('color', 'red');
            }
        };
        ajax(data,success,'/php/contract/contract_info.php');
        $('#header_text').html("Contract # <span id='selection'>"+id+"</span>");
    });
    $('#active_contracts').click(function (){
        var success = function (response) {
            $('#contracts_table').html(response);};
        data = "selection="+id+"&function=contracts_table&status=0";
        ajax(data,success,'/php/clients/client_dashboard.php');
        $(this).removeClass('btn-warning').addClass('btn-info');
        $('#completed_contracts').removeClass('btn-info').addClass('btn-warning');
    });
    $('#completed_contracts').click(function (){
        var success = function (response) {
            $('#contracts_table').html(response);};
        data = "selection="+id+"&function=contracts_table&status=1";
        ajax(data,success,'/php/clients/client_dashboard.php');
        $(this).removeClass('btn-warning').addClass('btn-info');
        $('#active_contracts').removeClass('btn-info').addClass('btn-warning');
    });
    $(".date_picker").change(function (){
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        success = function (response) {
            $('#activity_table').html(response);};
        data = "selection="+id+"&function=activity_table_refresh&date_from="+from_date+"&date_to="+to_date;
        ajax(data,success,'/php/clients/client_dashboard.php');
    });
    $('#contracts_table').on('click', 'table tbody tr', function() {
      var id = $("#info_container").children('h3').attr('id');
        var contract_id = $(this).children('td:first-child').html();
        console.log(id);
        data = 'ID='+contract_id;
        success = function (response){
            $('#container2').html(response);
            $("#container1").html("<button id='"+id+"' class='btn btn-info back_client text-center center-block'>Back to Client #"+id+"</button><button id='"+contract_id+"' class='view_contract btn btn-info text-center center-block'>View PDF</button>");
            $("#scripts").html("<script src='/js/contract_management/individual_contract.js'></script>");
            if($('#status').html() === 'Active'){
                $("#status").css('color', 'green');
            } else {
                $("#status").css('color', 'red');
            }
        };
        ajax(data,success,'/php/contract/contract_info.php');
        $('#header_text').html("Contract:  <span id='selection'> "+id+"</span>");
    })
    $(".all_contacts").click(function(){
      var id = $(this).attr('id');
      success = function (response) {
        $('#my_modal').modal('toggle');
        $("#modal_scripts").html("<script src='/js/client_management/edit_contacts.js'></script>");
        $("#modal_title").html('Client #'+id+' Contacts');
        $("#modal_body").html(response);
      };
      data = "ID="+id;
      ajax(data,success,'/php/clients/all_contacts.php');
    });
    $(".create_contact").click(function(){
      var id = $(this).attr('id');
      success = function (response) {
        $('#my_modal').modal('toggle');
        $("#modal_scripts").html("<script src='/js/client_management/contact_form.js'></script>");
        $("#modal_title").html('Create Contact');
        $("#modal_body").html(response);
      };
      data = "ID="+id;
      ajax(data,success,'/php/clients/contact_form.php');
    });
});
