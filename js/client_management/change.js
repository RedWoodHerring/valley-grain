$(function(){
  $(".submit").click(function(){
    var id = $(this).parent().parent().attr('id');
    var value = $(this).parent().prev('p').children('input').val();
    var selection = $(this).parent().prev().prev().html();
    var data = "ID="+id+"&change="+value+"&function="+selection;
    var success = function (response){
      console.log(response);
      $('#modal_msg').html("<p class='alert bg-success'>"+selection+" Edited</p>");
      success = function (response) {
        $("#modal_scripts").html("<script src='/js/client_management/edit_contacts.js'></script>");
        $("#modal_title").html('Client #'+id+' Contacts');
        $("#modal_body").html(response);
      };
      data = "ID="+id;
      ajax(data,success,'/php/clients/all_contacts.php');

    }
    ajax(data,success,'/php/clients/change.php');
  });
});
