$(function () {

    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            erase_modal();
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
    });
    $(".submit_name_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'name','Name','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_address_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'street_address','Address','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_city_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'city','City','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_state_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'state','State','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_zip_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'zip','ZIP','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_phone_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'phone','Phone','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
    $(".submit_type_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'type','Type','clients');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/client_management/client_edit_form_handler.js'></script>");
            $("#modal_title").html('Client #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('edit_client_modal',success,id);
        data = "selection=\*&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
    });
});