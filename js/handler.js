// FILE FOR HANDLING JAVASCRIPT/JQUERY REQUESTS AND GRABBING DATA FROM THE SITE TO PASS TO FUNCTIONS
$(function() {
    //      VARIABLES
    var users_button = $('#users');
    var clients_button = $('#clients');
    var products_button = $('#products');
    var tickets_button = $('#tickets');
    var carrier_button = $('#carriers');
    var bins_button = $('#bins');
    var sale_button = $('#sale');
    var analysis_button = $('#analysis');
    $('.logout-button').click(function () {
        var data = 'function=logout';
        var func_yes = function () {window.location.href = '../';};
        var func_no = function () {$('#err').html('Failed');};
        ajax_send_data(data,func_yes,func_no);
    });
    users_button.click(function (){
        data = 'selection=ID,f_name,l_name,email,Level&function=main_table&table=login&title=Create New User&script=/js/user_management/user_handler.js&id=new_user_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_user_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create User</button>");
        $('#header_text').html("Admin Tools > Users");
        $(".nav li").removeClass("selected");
        $('#containerdata').html('');
        $('#admin_tools').addClass('selected');
    });
    clients_button.click(function (){
        data = "selection=ID,name,phone,type&function=main_table&table=clients&title=Create New Client&script=/js/client_management/client_handler.js&id=new_client_button";
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_client_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Client</button>");
        $('#header_text').html("Clients");
        $(".nav li").removeClass("selected");
        clients_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    tickets_button.click(function (){
        selection = 'ID,date,product_id,client_id,dockage_price,bin_id,net_bushels';
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_ticket_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Ticket</button>");
        $('#header_text').html("Tickets");
        $(".nav li").removeClass("selected");
        tickets_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    products_button.click(function (){
        selection = 'ID,product_name';
        data = 'selection='+selection+'&function=main_table&table=Products&title=Create New Product&script=/js/products_management/main.js&id=new_product_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_product_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Product</button>");
        $('#header_text').html("Products");
        $(".nav li").removeClass("selected");
        products_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    carrier_button.click(function (){
        selection = 'ID,name,s_address,city,state,zip,phone';
        data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Carrier&script=/js/carrier_management/main.js&id=new_carrier_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_carrier_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Carrier</button>");
        $('#header_text').html("Carriers");
        $(".nav li").removeClass("selected");
        carrier_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    bins_button.click(function (){
        selection = 'ID,location';
        data = 'selection='+selection+'&function=main_table&table=bins&title=Create New Bin&script=/js/bin_management/main.js&id=new_bin_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_bin_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Bin</button>");
        $('#header_text').html("Bins");
        $(".nav li").removeClass("selected");
        bins_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    sale_button.click(function (){
        selection = 'id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id';
        data = 'selection='+selection+'&function=main_table&table=sale&title=Create New Sale&script=/js/sale_management/main.js&id=new_sale_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html("<button id='new_sale_button' class='btn btn-default justify-center'>Create Bill of Lading</button>");
        $('#header_text').html("Bill of Lading");
        $(".nav li").removeClass("selected");
        sale_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    analysis_button.click(function (){
        selection = 'ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin';
        data = 'selection='+selection+'&function=main_table&table=analysis&title=Create New analysis&script=/js/analysis_management/main.js&id=new_analysis_button';
        ajax_generation(data,data_table_reload);
        $("#container1").html('');
        $('#header_text').html("Analysis");
        $(".nav li").removeClass("selected");
        analysis_button.addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
    $("#contracts").click(function (){
        data = '';
        success = function (response){
            $('#scripts').html("<script src='/js/contract_management/main.js'></script>");
            $('#container1').html("<button class='btn btn-primary' id='new_contract'>New Contract</button>");
            $('#container2').html(response);
            $("#container2 table").DataTable();
        };
        ajax(data,success,'../php/contract/contract_list.php');
        $("#container1").html("<button id='new_contract_button' class='btn btn-default justify-center' data-toggle='modal' data-target='#my_modal'>Create Contract</button>");
        $('#header_text').html("Contract");
        $(".nav li").removeClass("selected");
        $("#contracts").addClass('selected');
        $('#admin_sub_menu').removeClass('show');
        $('#containerdata').html('');
    });
});
