$(function(){
  $('.submit_product_name').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=Products&test=simple_test&column=product_name&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Name Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){$('#modal_msg').html("<p class='alert bg-warning'>Name Taken, Please Try Again</p>").fadeIn(1200).fadeOut(1200)};
    ajax_send_data(data,func_yes,func_no);
    selection = 'ID,product_name';
    data = 'selection='+selection+'&function=main_table&table=Products&title=Create New Product&script=/js/products_management/main.js&id=new_product_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/products_management/edit_form.js'></script>");
        $("#modal_title").html('Product #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=products_options&selection=Products";
    ajax_generation(data, success);
  });
});
