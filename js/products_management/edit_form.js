$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().attr('id');
        options_form(id,value,'/js/products_management/change.js',submit,'Product Options',button_container);
    });
    $(".delete").click(function (){
        var id = $(this).attr('id');
        data = 'selection='+id+'&function=delete&table=Products';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>Product Deleted</p>").fadeIn(900).fadeOut(900)};
        func_no = function (){$('#modal_msg').html("<p class='alert bg-danger'>Product cannot be deleted, please contact an admin</p>").fadeIn(900).fadeOut(900)};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,product_name';
        data = 'selection='+selection+'&function=main_table&table=Products&title=Create New Product&script=/js/products_management/main.js&id=new_product_button';
        ajax_generation(data,data_table_reload);
        erase_modal();
        $('#my_modal').modal('toggle');
    });
});
