$(function (){
    $('#submit_new_product').click(function (){
        var data = $('#new_product_form').serialize()+'&function=create&selection=product&table=Products';
        func_yes = function (response) {

            if(response === 'already created'){
              $('#modal_msg').html("<p class='alert bg-warning text-center'>Product Already Created</p>").fadeIn(1200).fadeOut(1200);
              $('#modal_footer').html('');
            } else {
              $('#modal_msg').html("<p class='alert bg-success text-center'>Product Added</p>").fadeIn(1200).fadeOut(1200);
              $('#modal_body').html("<button id='new_product_button' class='btn btn-default justify-center'>Create New Product</button>");
              $('#modal_footer').html('');
            }
        };
        func_no = function (response) {
          $('#modal_msg').html("<p class='alert bg-warning text-center'>Product Already Created</p>").fadeIn(1200).fadeOut(1200);
          $('#modal_footer').html('');
        };
        ajax_send_data(data,func_yes,func_no);
        var selection = 'ID,product_name';
        data = 'selection='+selection+'&function=main_table&table=Products&title=Create New Product&script=/js/products_management/main.js&id=new_product_button';
        ajax_generation(data,data_table_reload);
    })
});
