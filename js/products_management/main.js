$(function() {
    $("#new_product_button").click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/products_management/submit_new.js'></script>");
            $("#modal_title").html('Create a New Product');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_product' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=product&function=create_new_modal";
        ajax_generation(data, success);
    });
    $('#table_id').on('click', 'tbody tr', function() {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/products_management/edit_form.js'></script>");
            $("#modal_title").html('Product #'+id+' Options');
            $("#modal_body").html(response);
            $('#my_modal').modal('toggle');
        };
        data = "id="+id+"&function=products_options&selection=Products";
        ajax_generation(data, success);
    });
});
