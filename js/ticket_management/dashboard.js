$(function(){
  $('.back_contract').click(function (){
      var id = $(this).attr('id');
      data = 'ID='+id;
      success = function (response){
          $('#container2').html(response);
          $('#container1').html("<button class='btn btn-primary back'>Back</button><button id='"+id+"' class='btn btn-info view_contract'>View Contract</button>");
          $("#scripts").append("<script src='/js/contract_management/individual_contract.js'></script>");
          if($('#status').html() === 'Active'){
              $("#status").css('color', 'green');
          } else {
              $("#status").css('color', 'red');
          }
      };
      ajax(data,success,'/php/contract/contract_info.php');
      $('#header_text').html("Contract # <span id='selection'>"+id+"</span>");
  });
  $('.back_client').click(function (){
      var id = $(this).attr('id');
      var name = $("#client_info").children("div:first-child").children('h7').html();
      $('#styles').attr('href','/css/client_dashboard.css');
      $('#header_text').html("Client: "+name);
      $('#container1').html("<button class='btn btn-primary back center-block'>Back to Clients</button>");
      var success = function (response) {
        $('#container2').html(response);
        $('#info_container').children('h3').attr('id',id);
        var success = function (response) {
            $('#contracts_table').html(response);$("#full_contracts_table").DataTable();};
        data = "selection="+id+"&function=contracts_table&status=0";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#activity_table').html(response);};
        data = "selection="+id+"&function=activity_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        success = function (response) {
            $('#contacts_table').html(response);};
        data = "selection="+id+"&function=contract_table_initial";
        ajax(data,success,'../php/clients/client_dashboard.php');
        $('#scripts').html("<script src='/js/client_management/client_dashboard.js'></script>");
      };
      data = "selection="+id+"&function=client_dashboard";
      ajax(data,success,'/php/clients/client_dashboard.php');
  });
});
