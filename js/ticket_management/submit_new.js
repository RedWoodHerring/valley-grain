$(function() {
    $("#Products").addClass('contract_identifier').addClass('product_value');
    $("#Client").addClass('contract_identifier').addClass('client_value');

    $(".contract_identifier").change(function (){
        var client = $(".client_value").val();
        var product = $(".product_value").val();
        if (product === "--Please Select--"){
            $("#modal_msg").html("<p class='alert bg-success'>Please Select a Client</p>").fadeIn(800).fadeOut(800);
        }else if (client === "--Please Select--"){
            $("#modal_msg").html("<p class='alert bg-success'>Please Select a Product</p>").fadeIn(800).fadeOut(800);
        } else {
            var success = function (response) {
                $("#contract_container").html(response);
            };
            data = "function=ticket_contract&client=" + client + "&product=" + product;
            ajax(data, success, '/php/handler.php');
        }
    });

    $("#new_temp_contract").click(function (){
        $("#new_user_form").css('display', 'none');
    });

    $("#contract").change(function (){
        console.log('worked');
        var success = function(response){

        };
        data = "function=ticket_form";
        ajax(data, success, '/php/tickets/new_form.php');
    });


});