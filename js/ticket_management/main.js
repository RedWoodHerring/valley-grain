$(function() {
    $("#new_ticket_button").click(function () {
        success = function (response) {
            $("#modal_scripts").html("<script src='/js/ticket_management/submit_new.js'></script>");
            $("#modal_title").html('Submit a New Ticket');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='submit_new_ticket' class='btn btn-primary'>Submit</button>");
        };
        data = "selection=ticket&function=create_new_modal";
        ajax_generation(data, success);
    });
    $('#table_id').on('click', 'tbody tr', function() {
        var id = $(this).attr('id');
        success = function (response) {
            $("#header_text").html('Ticket #'+id);
            $("#container1").html("<button id='"+id+"' class='btn btn-info view_button text-center center-block' data-toggle='modal' data-target='#my_modal'>View</button>");
            $("#container2").html(response);
            $("#scripts").html("<script src='/js/ticket_management/dashboard.js'></script>");
        };
        data = "ID="+id;
        ajax(data,success,'/php/tickets/dashboard.php');
    });
    $(".view_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            $("#modal_title").html('Ticket #'+id+' Measurements');
            $("#modal_body").html(response);
        };
        ajax_selector_generation('ticket_measurements',success,id);
    });
});
