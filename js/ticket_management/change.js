$(function () {
    $(".options_button").click(function () {
        var id = $(this).attr('id');
        success = function (response) {
            erase_modal();
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
            $("#modal_footer").html("<button id='"+id+"' class='btn btn-danger delete'>Delete</button>");
        };
        data = "id="+id+"&function=ticket_options&selection=ticket";
        ajax_generation(data, success);

    });
    var table = 'tickets';
    $(".submit_date_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'date','Date',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        selection = 'ID,date,product_id,client_id,dockage_price,bin_id,net_bushels';
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_product_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'product_id','Product',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_client_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'client_id','Client',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_protein_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'protein','Protein',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_moisture_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'moisture','Moisture',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_test_wt_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'test_weight','Test Weight',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_fm_dockage_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'dockage','FM/Dockage',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_plump_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'plump_thin','Plump - Thin',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_vom_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'vom','VOM',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_cofo_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'cofo','COFO',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_dockage_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'dockage_price','Total Dockage',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_bin_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'bin_id','Bin',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_bushels_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'net_bushels','Bushels',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_comment_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'comment','Comment',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });
    $(".submit_other_value").click(function () {
        var id = $(this).attr('id');
        change_option(id,'other','Other',table);
        data = "id="+id+"&function=ticket_options&selection=ticket";
        success = function (response) {
            $('#modal_footer').html('');
            $("#modal_scripts").html("<script src='/js/ticket_management/edit_form.js'></script>");
            $("#modal_title").html('Ticket #'+id+' Options');
            $("#modal_body").html(response);
        };
        ajax_generation(data, success);
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
    });


});