$(function () {
    $(".edit").click(function () {
        var id = $(this).attr('id');
        var button_container = $(this).parent();
        var value = $(this).parent().prev().attr('id');
        var submit = "submit_"+$(this).parent().prev().attr('id');
        options_form(id,value,'/js/ticket_management/change.js',submit,'Product Options',button_container);
        $("#modal_footer").html('');
    });
    $(".delete").click(function (){
        id =  $(this).attr('id');
        data = 'selection='+id+'&function=delete&table=tickets';
        func_yes = function(){$('#err').html("<p class='alert bg-success'>Ticket Deleted</p>")};
        func_no = function (){$('#err').html("<p class='alert bg-danger'>Ticket cannot be deleted, please contact an admin</p>")};
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,date,product_id,client_id,dockage_price,bin_id,net_bushels';
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
        erase_modal();
        $('#my_modal').modal('toggle');
        $('#err').fadeOut(5000);
    });
});