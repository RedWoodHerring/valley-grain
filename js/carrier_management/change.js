$(function(){
  $('.submit_name').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&test=simple_test&column=name&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Name Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){$('#modal_msg').html("<p class='alert bg-warning'>Name Taken, Please Try Again</p>").fadeIn(1200).fadeOut(1200)};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
  $('.submit_s_address').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&column=s_address&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Street Address Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
  $('.submit_city').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&column=city&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>City Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
  $('.submit_state').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&column=state&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>State Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
  $('.submit_zip').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&column=zip&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>ZIPCODE Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
  $('.submit_phone').click(function (){
    var id = $(this).attr('id');
    var change = $("#change").val();
    var data = 'function=update_table&selection='+id+'&table=carriers&column=phone&change='+change;
    var func_yes = function(){$('#modal_msg').html("<p class='alert bg-success'>Phone Number Changed</p>").fadeIn(1200).fadeOut(1200)};
    var func_no = function (){};
    ajax_send_data(data,func_yes,func_no);
    selection = '*';
    data = 'selection='+selection+'&function=main_table&table=carriers&title=Create New Client&script=/js/carrier_management/main.js&id=new_carrier_button';
    ajax_generation(data,data_table_reload);
    success = function (response) {
        $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
        $("#modal_title").html('Carrier #'+id+' Options');
        $("#modal_body").html(response);
    };
    data = "id="+id+"&function=carrier_options&selection=Carriers";
    ajax_generation(data, success);
  });
});
