$(function(){
  $('#table_id').on('click', 'tbody tr', function() {
      var id = $(this).attr('id');
      success = function (response) {
          $("#modal_scripts").html("<script src='/js/carrier_management/edit_form.js'></script>");
          $("#modal_title").html('Carrier #'+id+' Options');
          $("#modal_body").html(response);
          $('#my_modal').modal('toggle');
      };
      data = "id="+id+"&function=carrier_options&selection=Carriers";
      ajax_generation(data, success);
  });
});
