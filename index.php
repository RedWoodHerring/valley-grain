<?php
session_start();
if (isset ($_SESSION['ID'])) {
    header("Location: portal/index.php");
} else {
    include 'php/var.php';
    echo $header_html;
}

 ?>
    <script src="js/functions.js"></script>
    <script src="js/handler.js"></script>
    <title>Valley Grain App</title>
</head>
<body>
    <div class="container-fluid" id="wrapper">
        <div class="row">
            <nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2 bg-faded sidebar-style-1">
                <h1 class="site-title"><a href="index.php"><em class="fa"></em>Valley Grain Milling</a></h1>
                <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
                <ul class="nav nav-pills flex-column sidebar-nav">
                    <!--TODO Test Email Script to Log-->
                    <li id="forgot_password" class="nav-item"><a class="nav-link active" href="#"><em class="fa fa-dashboard"></em>Forgot Password<span class="sr-only">(current)</span></a></li>
                    <li id="forgot_username" class="nav-item"><a class="nav-link" href=""><em class="fa fa-calendar-o"></em>Forgot Username</a></li>
                    <li class="nav-item"><a class="nav-link" href="http://www.valleygrainmilling.com/"><em class="fa fa-bar-chart"></em>Website</a></li>
                </ul>
            </nav>
            <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">

                <header class="page-header row justify-center">
                    <div class="col-md-6 col-lg-8" >
                        <h1 class="float-left text-center text-md-left">Login</h1>
                    </div>
                    <div class="clear"></div>
                </header>

                <?= $login_form; ?>

            </main>
        </div>
    </div>
</body>
</html>
