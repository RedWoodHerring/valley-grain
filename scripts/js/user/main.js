$(function(){
  $("#content_container_1").html("<button id='new_user' class='btn btn-primary'>Create User</button>");
  $("#search").html('');
  $("#content_container_3").html('');
  $("h1").html("Users");
  var data = 'column=ID&direction=ASC';
  var if_true = function(response){
    $("#data").html(response);
    $(".table tr td:nth-child(4)").hide();
    $(".table tr th:nth-child(4)").hide();
  }
  var if_false = function(){};
  ajax(data,if_true,if_false,'/scripts/php/user/main.php');
  $('#data').on('click','table thead tr th', function(){
    var selector = "#"+$(this).attr('id');
    if($(selector).hasClass('up')){
      var direction = 'DESC';
    } else {
      var direction = 'ASC';
    }
    data = "column="+$(this).attr('id')+"&direction="+direction;
    if_true = function(response){
      $("#data").html(response);
      if ($(selector).hasClass('sorted')){
      } else {
        $(selector).addClass('sorted');
      }
      if (direction === 'ASC'){
        $(selector).addClass('up');
      } else {
        $(selector).removeClass('up');
      }
    }
    ajax(data,if_true,if_false,'/scripts/php/user/main.php');
  });
  $('#data').on('click','table tbody tr', function(){
    var id = $(this).attr('id');
    var email = $(this).children('td:nth-child(2)').html();
    data = 'ID='+id;
    if_true = function(response){
      if(window.history.state.page === "Users"){
        window.history.pushState({page: "User Details"}, "User Details", "/user/details");
      } else{
        window.history.replaceState({page: "User Details"}, "User Details", "/user/details");
      }
      $("#content_container_1").html("<button class='btn btn-primary back'>Back</button>");
      $("h1").html(email);
      $("#data").html('');
      $("#content_container_3").html(response);
      $("#content_scripts").html("<script src='/scripts/js/user/details.min.js'></script>");
    };
    ajax(data,if_true,if_false,'/scripts/php/user/details.php');
    //window.history.replaceState({page: "Users"}, "Users Dashboard", "/user");
  });
  $("#new_user").on('click', function(){
    if_true = function(response){
      if(window.history.state.page === "Users"){
        window.history.pushState({page: "New User"}, "New User", "/user/new");
      } else{
        window.history.replaceState({page: "New User"}, "New User", "/user/new");
      }
      $("#content_container_1").html("<button class='btn btn-primary back'>Back</button>");
      $("h1").html('Create User');
      $("#data").html('');
      $("#content_container_3").html(response+"<button class='btn btn-primary submit'>Submit</button>");
      $("#content_scripts").html("<script src='/scripts/js/user/new.min.js'></script>");
    }
    ajax(data,if_true,if_false,'/scripts/php/user/new.php');
  });
});
