$(function(){
  $(".edit").on('click', function(){
    var column = $(this).parent().attr('id');
    var container = $(this).prev('h6');
    var value = $(this).prev('h6').html();
    var button = $(this);
    container.html("<input name='"+column+"' value='"+value+"'/>");
    button.html("Submit");
    button.removeClass("edit");
    button.addClass("submit");
    $("#user_data div").not("#"+column).hide();
  });
  $(".submit").on('click', function(){
    var change = $(this).prev('h6').children().val();
    var id = $(this).attr('id');
    var data = "ID="+id+"&change="+change;
  });
  $(".back").on('click', function(){
    var if_false = function(){};
    var id = $(".submit").attr('id');
    data = 'ID='+id;
    if_true = function(response){
      if(window.history.state.page === "Users"){
        window.history.pushState({page: "User Details"}, "User Details", "/user/details");
      } else{
        window.history.replaceState({page: "User Details"}, "User Details", "/user/details");
      }
      $("#content_container_1").html("<button class='btn btn-primary back'>Back</button>");
      $("#data").html('');
      $("#content_container_3").html(response);
      $("#content_scripts").html("<script src='/scripts/js/user/details.min.js'></script>");
    };
    ajax(data,if_true,if_false,'/scripts/php/user/details.php');
  });
});
