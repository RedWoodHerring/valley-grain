$(function(){
  $(".submit").on('click', function (){
    var data = $("#new_user_form").serialize();
    var if_false = function (response){console.log(response);};
    var if_true = function (response){
      if(response === "true"){
        $("#msg").html("<p class='success'>New User Created</p>");
        data = '';
        if_true = function(response){
          $("#content_container_3").html(response+"<button class='btn btn-primary submit'>Submit</button>");
        }
        ajax(data,if_true,if_false,'/scripts/php/user/new.php');
      }
      if(response === "already created"){
        $("#msg").html("<p class='warning'>User Already Exists, Please Try Again</p>");
      }
    }
    ajax(data,if_true,if_false, '/scripts/php/user/create.php');
  });
  $(".back").on('click', function (){
    window.history.pushState({page: "Users"}, "Users Dashboard", "/user");
    $("#content_scripts").html("<script src='/scripts/js/user/main.min.js'></script>");
    return false;
  });
});
