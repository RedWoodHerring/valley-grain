$(function(){
  history.pushState({page: "New Ticket"}, "Create a New Ticket", "/tickets/new");
  //$("#window_script").html("<script src='/scripts/js/window_change.min.js'></script>");
  $("#submit").click(function(){
    var data = $("#new_ticket").serialize();
    var if_true = function (response){
      $("#msg").html("<p class='success'>Ticket #"+response+" Created</p>");
    }
    var if_false = function(){
      $("#msg").html("<p class='success'>Ticket Unable To Be Created. Please Contact an Administrator</p>");
    }
    ajax(data,if_true,if_false,'/scripts/php/tickets/submit.php');
  });
});
