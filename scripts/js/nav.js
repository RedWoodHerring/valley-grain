$(function (){
    $("#user_main").click(function(){
        close_menu(event);
        window.history.pushState({page: "Users"}, "Users Dashboard", "/user");
        $("#content_scripts").html("<script src='/scripts/js/user/main.min.js'></script>");
        return false;
    });
    $("#products_main").click(function(){
        close_menu(event);
        var data = "function=products_main";
        var if_true = function(response){
            $("#user_dropdown").children('h2').html('');
            $("#header").html('<h1>Products</h1>');
            $("#content_container_1").html("<button class='btn btn-primary' id='new_user'>Create Product</button>");
            $("#data").html(response);
            $("#content_scripts").html("<script src='/scripts/js/user/product_table.min.js'></script>");
            $(".main_table").attr('id', 'product_table');
            $(".main_table tbody tr:nth-child(1n+6)").hide();
            $("#pagination").html("<button class='page'>1</button><button class='page'>2</button>");
            event.preventDefault();
        };
        var if_false = function(){};
        ajax(data,if_true,if_false, 'scripts/php/products/main.php');
        return false;
    });
});
