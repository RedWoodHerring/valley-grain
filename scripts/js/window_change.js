$(function (){
  window.onpopstate = function(event){
    var state = window.location.pathname;
    console.log(state)
    if(state === "/dashboard"){
      //window.history.replaceState({page: "Dashboard"}, "Valley Grain Dashboard", "/dashboard");
      $("h1").html("Dashboard");
      $("#content_container_1").html("<div class='row'><button class='btn btn-primary' id='create_ticket'>Create Ticket</button><button class='btn btn-primary' id='create_sale'>Create Bill of Lading</button></div>");
      var data = '';
      var if_true = function (response){$("#search").html(response)};
      var if_false = function (){};
      ajax(data,if_true,if_false,'/scripts/php/dashboard_search.php');
      if_true = function (response){$("#content_container_3").html(response)};
      ajax(data,if_true,if_false,'/scripts/php/recent.php');
      $("#data").html('');
      $("#content_scripts").html("<script src='/scripts/js/dashboard/main.min.js'></script>");
    }

    if(state === "/user" ){
      //window.history.replaceState({page: "Users"}, "Users Dashboard", "/user");
      $("#content_container_1").html('');
      $("#table_options_container").html('');
      $("#content_container_3").html('');
      $("h1").html("Users");
      $("#content_scripts").html("<script src='/scripts/js/user/main.min.js'></script>");
    }
    if(state === "/user/new" ){
      //window.history.replaceState({page: "Users"}, "Users Dashboard", "/user");
      var data = '';
      var if_true = function (response){$("#search").html(response)};
      var if_true = function(response){
        if(window.history.state.page === "Users"){
          window.history.pushState({page: "New User"}, "New User", "/user/new");
        } else{
          window.history.replaceState({page: "New User"}, "New User", "/user/new");
        }
        $("#content_container_1").html("<button class='btn btn-primary back'>Back</button>");
        $("h1").html('Create User');
        $("#data").html('');
        $("#content_container_3").html(response+"<button class='btn btn-primary submit'>Submit</button>");
        $("#content_scripts").html("<script src='/scripts/js/user/new.min.js'></script>");
      }
      ajax(data,if_true,if_false,'/scripts/php/user/new.php');
    }
  }
});
