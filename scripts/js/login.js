$(function (){
  var state_object = {login: "login"};
  window.history.pushState(state_object, "Valley Grain Dashboard Login", "/login");
    $("h1").html("Login");
    $('#submit').click(function (){
        var data = $("#login_form").serialize() + '&function=login';
        var email = $("#email").val();
        var if_true = function () {
          var state_object = {page: "Dashboard"};
            window.history.pushState({page: "Dashboard"}, "Valley Grain Dashboard", "/dashboard");
            $("h1").html("Dashboard");
        };
        var if_false = function (response1) {
            $("#form_msg").html("<h2>" + response1 + "</h2>");
        };
        ajax(data, if_true, if_false, '/scripts/php/login.php');
    })
});
