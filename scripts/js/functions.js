
var ajax = function(data,if_true,if_false,url){
    $.ajax({
        url: url,
        data: data,
        type: 'post',
        async: true,
        success: function(response) {
            if (response){
                if_true(response);
            } else {
                if_false(response);
            }
        }
    });
};
var close_menu = function (ev){
    ev.preventDefault();
    $("#menu_button").removeClass('open');
    $("nav").css('left', '-75%');
    $("#logo_container").css({'box-shadow': '#000 0 0 .5em','background-color':'#D49A36','border-radius': '0'});
    $("#user_image").show();
    $("#close_button").hide().css('display', 'none');
};

var window_reload = function (){
  window.onpopstate = function(event){
    var state = JSON.stringify(event.state.page);
    return state;
  }
}
