$(function (){
    var menu_button = $("#menu_button");
    $("#menu_button").click(function (){
        if ($(this).hasClass('open') === true){
            $(this).removeClass('open');
            $("nav").css('left', '-75%');
            $("#logo_container").css({'box-shadow': '#000 0 0 .5em','background-color':'#D49A36','border-radius': '0'});
            $("#user_image").show();
            $("#close_button").hide();
            $("#close_button").show().css('display', 'none');
        } else {
            $(this).addClass('open');
            $("nav").css('left', '0');
            $("#user_image").hide();
            $("#close_button").show().css('display', 'flex');
        }
    });
    $(".menu_item").children('h3').click(function (){
        if($(this).parent().hasClass('open') === true){
            $(this).parent().removeClass('open');
            $(this).next('div').children().animate({height: '0',padding: '0'},200);
            $(this).next('div').children().css('border-bottom', 'none');
            $(this).next('div').children().css('border-top', 'none');
        } else {
            $(this).parent().addClass('open');
            $(this).next('div').children().animate({height: '1.5em',padding: '.5em 0'},200);
            $(this).next('div').children().css('border-bottom', '.1em rgba(28,56,76,.5) solid');
            $(this).next('div').children().css('border-top', '.1em rgba(28,56,76,.5) solid');
        }
    });
    menu_button.mouseenter(function (){
        if ($("#menu_button").hasClass('') === true) {
            $("#user_image").fadeTo("fast", .7);
        }
    });
    menu_button.mouseleave(function (){
        if ($("#menu_button").hasClass('') === true) {
            $("#user_image").fadeTo("fast", 1);
        }
    })

});