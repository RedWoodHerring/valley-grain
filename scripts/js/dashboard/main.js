$(function(){
  window.history.replaceState({page: "Dashboard"}, "Valley Grain Dashboard", "/dashboard");
  //$("#window_script").html("<script src='/scripts/js/window_change.min.js'></script>");
  


  $('#search_box_container').children().keyup(function(){
    console.log('worked');
    var input = $(this).val();
    if (input === ''){
      $('#data').html('');
    } else {
      var table = $("#search_form").serialize();
      var data = "input="+input+"&"+table;
      var if_true = function (response){
        $('#data').html(response);
        //console.log(response);
      }
      var if_false = function (response){
        console.log(response);
      }
      ajax(data,if_true,if_false,'/scripts/php/search.php');
    }
    });
    $('.form_group input').change(function(){
      var input = $('#search_box_container').children().val();
      if (input === ''){
        $('#data').html('');
      } else {
        var table = $("#search_form").serialize();
        var data = "input="+input+"&"+table;
        var if_true = function (response){
          $('#data').html(response);
          //console.log(response);
        }
        var if_false = function (response){
          console.log(response);
        }
        ajax(data,if_true,if_false,'/scripts/php/search.php');
      }
      });

    $("#create_ticket").click(function (){
      var data = '';
      var if_true = function (response){
        $("h1").html('Create Ticket');
        $("#content_container_1").html(response);
        $("#search_container").html('');
        $("#content_scripts").html("<script src='/scripts/js/tickets/new.js'></script>");

        //history.pushState({page: 'New Ticket'}, "Create New Ticket", "/tickets/new");
      }
      var if_false = function (response){
        console.log(response);
      }
      ajax(data,if_true,if_false,'/scripts/php/tickets/new_form.php');
    });
    $("#create_sale").click(function (){
      var data = '';
      var if_true = function (response){
        $("h1").html('Create Bill of Lading');
        $("#content_container_1").html(response);
        $("#content_container_2").html('');
        $("#content_container_3").html('');
        $("#content_scripts").html("<script src='/scripts/js/tickets/new.js'></script>");
        history.replaceState({page: 'New Bill of Lading'}, "New Bill of Lading", "/sale/new");
      }
      var if_false = function (response){
        console.log(response);
      }
      ajax(data,if_true,if_false,'/scripts/php/sale/new_form.php');
    });
});
