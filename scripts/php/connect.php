<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/7/17
 * Time: 11:26 AM
 */

define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'homestead');
$db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);