<?php

require '../functions.php';

?>

<form id="new_user_form">
  <div class="form_group">
    <h5>First Name</h5>
    <input name='f_name'/>
  </div>
  <div class="form_group">
    <h5>Last Name</h5>
    <input name='l_name'/>
  </div>
  <div class="form_group">
    <h5>Email</h5>
    <input type="email" name='email'/>
  </div>
  <div class="form_group">
    <h5>Level</h5>
    <select name="level">
      <option value="1">Viewer</option>
      <option value="2">Editor</option>
      <option value="3">Administrator</option>
    </select>
  </div>
  <div class="form_group">
    <h5>Password</h5>
    <input type="password" name='password'/>
  </div>
</form>
