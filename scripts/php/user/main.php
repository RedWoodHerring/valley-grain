<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/8/17
 * Time: 4:24 PM
 */

require '../functions.php';
$sort = $_POST['column'];
$direction = $_POST['direction'];
$query = "SELECT login.ID,login.email,login.f_name,l_name,Level FROM login ORDER By $sort $direction";

$user_array = retrieve_data($query,1);
?>
<table id='user_table' class='table'>
  <thead>
    <tr>
      <th id="ID">User ID</th>
      <th id="email">Email</th>
      <th id="f_name">First Name</th>
      <th id="l_name">Last Name</th>
      <th id='Level'>Level</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($user_array as $section):?>
      <tr id="<?= $section['ID']?>">
        <?php foreach($section as $value):?>
          <td><?=$value?></td>
        <?php endforeach ?>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
