<?php

require '../functions.php';
$user_id = $_POST['ID'];
$query = "SELECT login.ID,login.email,login.f_name,login.l_name,levels.level_type FROM login JOIN levels ON login.Level = levels.ID WHERE login.ID='$user_id'";
$user_array = retrieve_data($query,1);
?>

<div id="user_data">
  <div id="email">
    <h4>Email:</h4>
    <h6><?=$user_array[0]['email']?></h6>
    <button id="<?=$user_array[0]['ID']?>" class="btn btn-primary edit">Edit</button>
  </div>
  <div id="f_name">
    <h4>First Name:</h4>
    <h6><?=$user_array[0]['f_name']?></h6>
    <button id="<?=$user_array[0]['ID']?>" class="btn btn-primary edit">Edit</button>
  </div>
  <div id="l_name">
    <h4>Last Name:</h4>
    <h6><?=$user_array[0]['l_name']?></h6>
    <button id="<?=$user_array[0]['ID']?>" class="btn btn-primary edit">Edit</button>
  </div>
  <div id="Level">
    <h4>Level:</h4>
    <h6><?=$user_array[0]['level_type']?></h6>
    <button id="<?=$user_array[0]['ID']?>" class="btn btn-primary edit">Edit</button>
  </div>
</div>
