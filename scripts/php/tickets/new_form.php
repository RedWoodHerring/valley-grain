<?php
require '../functions.php';
$product_array = retrieve_data("SELECT * FROM Products",1);
$clients_array = retrieve_data("SELECT * FROM clients",1);
$bin_array = retrieve_data("SELECT * FROM bins",1);
?>
<form id='new_ticket'>
  <div class="form_group">
    <h5>Date</h5>
    <input type="date" name="date" value="<?= date("Y-m-d")?>"/>
  </div>
  <div class="form_group">
    <h5>Product</h5>
    <select name="select">
      <option>Select One</option>
    <?php foreach($product_array as $value): ?>
      <option value='<?=$value['ID']?>'><?=$value['product_name']?></option>
    <?php endforeach?>
    </select>
  </div>
  <div class="form_group">
    <h5>Client</h5>
    <select name="select">
      <option>Select One</option>
    <?php foreach($clients_array as $value): ?>
      <option value='<?=$value['ID']?>'><?=$value['name']?></option>
    <?php endforeach?>
    </select>
  </div>
  <div>
    <h5 class="section_header">Analysis</h5>
    <table class="table">
      <thead>
        <tr>
          <th>Property</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Protein(%)</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>Moisture(%)</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>Test Weight</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>FM/Dockage</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>Plump-Thin</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>VOM(ppm)</td>
          <td><input type="number" name="protein"/></td>
        </tr>
        <tr>
          <td>COFO</td>
          <td>
            <select name="cofo">
              <option value="0">NO</option>
              <option value="1">Yes</option>
            </select>
          </td>
        </tr>
    </table>
  </div>
  <div class="form_group">
    <h5>Bin</h5>
    <select name="select">
      <option>Select One</option>
    <?php foreach($bin_array as $value): ?>
      <option value='<?=$value['ID']?>'><?=$value['location']?></option>
    <?php endforeach?>
    </select>
  </div>
  <div>
    <h5 class="section_header">Scale Area</h5>
  </div>
  <div class="form_group">
    <h5>Total Dockage</h5>
    <input type="number" name="total_dockage"/>
  </div>
  <div class="form_group">
    <h5>Net Weight</h5>
    <input type="number" name="total_dockage"/>
  </div>
  <div class="form_group">
    <h5>Weight Measurement</h5>
      <select name="measurement_type">
        <option value="0">Bushels</option>
        <option value="1">Tons</option>
      </select>
    </span>
  </div>
</form>
<button id='submit' class="btn btn-primary">Submit</button>
