<?php
require 'functions.php';
$date_from = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
$date_to = date("Y-m-d");
$query = "SELECT tickets.ID,tickets.date,clients.name,Products.product_name FROM tickets JOIN clients ON tickets.client_id = clients.ID JOIN Products ON tickets.product_id = Products.ID WHERE tickets.date BETWEEN '$date_from' and '$date_to'";
$tickets_array = retrieve_data($query,1);
$query = "SELECT sale.ID,sale.date,clients.name,Products.product_name FROM sale JOIN clients ON sale.client_id = clients.ID JOIN Products ON sale.product_id = Products.ID WHERE sale.date BETWEEN '$date_from' and '$date_to'";
$sale_array = retrieve_data($query,1);
?>
<h4>Recent Activity</h4>
<h6>Tickets</h6>
<table class="table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Date</th>
      <th>Client</th>
      <th>Product</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($tickets_array as $section):?>
      <tr id="<?= $section['ID']?>">
      <?php foreach($section as $value):?>
        <td><?=$value?></td>
      <?php endforeach ?>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
<h6>Bill of Lading</h6>
<table class="table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Date</th>
      <th>Client</th>
      <th>Product</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($sale_array as $section):?>
      <tr id="<?= $section['ID']?>">
      <?php foreach($section as $value):?>
        <td><?=$value?></td>
      <?php endforeach ?>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
