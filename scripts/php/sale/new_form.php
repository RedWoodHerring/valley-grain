<?php
require '../functions.php';
$product_array = retrieve_data("SELECT * FROM Products",1);
$clients_array = retrieve_data("SELECT * FROM clients WHERE status = '1' OR status='2'",1);
$bin_array = retrieve_data("SELECT * FROM bins",1);
$carrier_array = retrieve_data("SELECT * FROM carriers",1);
?>

<form id='new_sale'>
  <div class="form_group">
    <h5>Date</h5>
    <input type="date" name="date" value="<?= date("Y-m-d")?>"/>
  </div>
  <div class="form_group">
    <h5>Buyer</h5>
    <select name="client">
      <option>Select One</option>
    <?php foreach($clients_array as $value): ?>
      <option value='<?=$value['ID']?>'><?=$value['name']?></option>
    <?php endforeach?>
    </select>
  </div>
  <div id="contract_container" class="form_group">
  </div>
  <div class='hidden'>
    <div class="form_group">
      <h5>Delivery Number</h5>
      <input type="number" name="delivery"/>
    </div>
    <div class="form_group">
      <h5>Net Weight</h5>
      <input type="number" name="date" value="<?= date("Y-m-d")?>"/>
    </div>
    <div id='measurement' class="form_group">
      <h5>Measurement</h5>
    </div>
    <div class="form_group">
      <h5>Destination -- If Different From Buyer Address</h5>
      <input name="destination"/>
    </div>
    <div class="form_group">
      <h5>Freight to be Paid By</h5>
      <input name="f_paid"/>
    </div>
    <div class="form_group">
      <h5>Freight Rate</h5>
      <input type="number" name="f_rate"/>
    </div>
    <div class="form_group">
      <h5>Seal Numbers</h5>
      <input name="seal"/>
      <button class=" btn btn-primary addtional_seal">Add Seal Number</button>
    </div>
    <div class="form_group">
      <h5>Product</h5>
      <select name="product">
        <option>Select One</option>
      <?php foreach($product_array as $value): ?>
        <option value='<?=$value['ID']?>'><?=$value['product_name']?></option>
      <?php endforeach?>
      </select>
    </div>
    <div>
      <h5 class="section_header">Analysis</h5>
      <table class="table">
        <thead>
          <tr>
            <th>Property</th>
            <th>Value</th>
            <th>Test Frequency</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Protein (%)</td>
            <td><input type="number" name="protein"/></td>
            <td><select name="t_protein"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Moisture (%)</td>
            <td><input type="number" name="moisture"/></td>
            <td><select name="t_moisture"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Test Weight</td>
            <td><input type="number" name="test_wt"/></td>
            <td><select name="t_test_wt"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Vomitoxin (ppm)</td>
            <td><input type="number" name="vom"/></td>
            <td><select name="t_vom"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Fat (%)</td>
            <td><input type="number" name="fat"/></td>
            <td><select name="t_fat"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Fiber (%)</td>
            <td><input type="number" name="fiber"/></td>
            <td><select name="t_fiber"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Ash (%)</td>
            <td><input type="number" name="ash"/></td>
            <td><select name="t_ash"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
          <tr>
            <td>Aflatoxin (ppb)</td>
            <td><input type="number" name="a_toxin"/></td>
            <td><select name="t_a_toxin"><option value="0">Each Quarter</option><option value="1">Each Lot</option></select></td>
          </tr>
      </table>
    </div>
    <div class="form_group">
      <h5>Carrier</h5>
      <select name="carrier">
        <option>Select One</option>
      <?php foreach($carrier_array as $value): ?>
        <option value='<?=$value['ID']?>'><?=$value['name']?></option>
      <?php endforeach?>
      </select>
    </div>
    <div class="form_group">
      <h5>Transportation</h5>
      <select name="transportation">
        <option value="0">Truck</option>
        <option value="1">Trailer</option>
      </select>
      <input class="hidden" name="transport_no" type="number"/>
    </div>
    <div class="form_group">
      <h5>Bin</h5>
      <select name="bin">
        <option>Select One</option>
      <?php foreach($bin_array as $value): ?>
        <option value='<?=$value['ID']?>'><?=$value['location']?></option>
      <?php endforeach?>
      </select>
    </div>
    <div class="form_group">
      <h5>Comments/h5>
      <textarea name="comments"/>
    </div>
  </div>
</form>
<button id='submit' class="btn btn-primary">Submit</button>
