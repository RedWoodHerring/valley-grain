<?php
require '../functions.php';
//Ticket Info
$date = $_POST['date'];
$product = $_POST['product'];
$client = $_POST['client'];
$bin = $_POST['bin'];
$weight = $_POST['weight'];
$measurement = $_POST['measurement'];

//Analysis
$protein = $_POST['protein'];
$t_protein = $_POST['t_protein'];
$moisture = $_POST['moisture'];
$t_moisture = $_POST['t_moisture'];
$test_wt = $_POST['test_wt'];
$t_test_wt = $_POST['t_test_wt'];
$fat = $_POST['fat'];
$t_fat = $_POST['t_fat'];
$fiber = $_POST['fiber'];
$t_fiber = $_POST['t_fiber'];
$vom = $_POST['vom'];
$t_vom = $_POST['t_vom'];
$t_ash = $_POST['t_ash'];
$t_a_toxin = $_POST['t_a_toxin'];

//Submit to Tickets Table
$columns = "date,product_id,client_id,bin_id,dockage,weight,measurement";
$change = "'$date','$product','$client','$bin','$dockage','$weight','$measurement'";

$query = "INSERT INTO tickets ($columns) VALUES ($change)";

$last_id = new_row($query,0);

//Submit to Ticket Analysis Table
$columns = "ticket_id,protein,moisture,test_wt,fm_dockage,plump_thin,vom,cofo";
$change = "'$last_id,'$protein','$moisture','$test_wt','$fm','$plump','$vom','$cofo'";

$query = "INSERT INTO tickets ($columns) VALUES ($change)";

new_row($query,0);

// TODO: Create script to make pdf and store in every ticket and for printing or emailing to customer

echo $last_id;
