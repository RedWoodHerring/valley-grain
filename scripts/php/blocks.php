<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/7/17
 * Time: 11:20 AM
 */

$nav = <<<HTMLBLOCK
<nav>
    <div class="menu_item">
        <h3>Admin Tools</h3>
        <div class="submenu">
            <h5 id="user_main" class="sub_item">Users</h5>
            <h5 id="products_main" class="sub_item">Products</h5>
            <h5 class="sub_item">Carriers</h5>
            <h5 class="sub_item">Bins</h5>
        </div>
    </div>
    <div class="menu_item">
        <h3>Clients</h3>
    </div>
    <div class="menu_item">
        <h3>Contracts</h3>
    </div>
    <div class="menu_item">
        <h3>Tickets</h3>
        <div class="submenu">
            <h5 class="sub_item">Dashboard</h5>
            <h5 class="sub_item">Listing</h5>
            <h5 class="sub_item">Product Analysis</h5>
        </div>
    </div>
    <div class="menu_item">
        <h3>Bill of Lading</h3>
        <div class="submenu">
            <h5 class="sub_item">Dashboard</h5>
            <h5 class="sub_item">Listing</h5>
            <h5 class="sub_item">Product Analysis</h5>
        </div>
    </div>
    <div class="menu_item">
        <h3>Time Portal</h3>
    </div>
    <div class="menu_item">
        <h3>Reports</h3>
    </div>
    <div id="sign_out_container">
        <h3>Sign Out</h3>
    </div>
    <script src="/scripts/js/nav.min.js"></script>
</nav>
HTMLBLOCK;

$login_form = <<<HTMLBLOCK
<form id="login_form">
    <div class="form_group">
        <h3>Email</h3>
        <input id="email" type="email" name="email">
    </div>
    <div class="form_group">
        <h3>Password</h3>
        <input id="pwd" type="password" name="pwd">
    </div>
</form>
<button class="login_button" id="submit">Submit</button>
<button class="login_button" id="f_password">Forgot Password</button>
<div id="form_msg"></div>
<script src='scripts/js/login.js'></script>
HTMLBLOCK;

$new_user_form = <<<HTMLBLOCK
<form class="form-group col center-block" action="" method="post" id="new_user_form">
    <div class="row">
        <h5 class="col">Email</h5>
        <input class="col" type="email" title="username" name="email" id="email" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">First Name</h5>
        <input class="col" title="username" name="email" id="email" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Last Name</h5>
        <input class="col" title="username" name="l_name" id="l_name" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Password</h5>
        <input type="password" class="col" title="password" name="password" id="password" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Level</h5>
        <select class="col" name="level" id="level">
                <option value="1">Viewer</option>
                <option value="2">Publisher</option>
                <option value="3">Administrator</option>
            </select>
    </div>
    <hr>
</form>
HTMLBLOCK;
