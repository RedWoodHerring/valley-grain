<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/7/17
 * Time: 11:54 AM
 */

require_once 'connect.php';
session_start();

function new_row($query,$check){
  global $db;
  if($check === 0){
    if(mysqli_query($db, $query)){
      $last_id = mysqli_insert_id($db);
      return $last_id;
    }
  }
}


function check ($query) {
    global $db;
    $check = mysqli_num_rows(mysqli_query($db,$query));
    if ($check === 1){
        return true;
    }
    return false;
};

function retrieve_data ($query,$check){
    global $db;
    $array = array();
    $result = mysqli_query($db,$query);
    if($check === 0){
        $array = mysqli_fetch_assoc($result);
        return $array;
    } else {
      $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
      return $array;
    }
}

function horizontal_table ($check,$array,$headers,$column_sort,$sort){
    $html = "<div id='search_container'><select id='limit'><option value='5'>5</option><option value='10'>10</option><option value='15'>15</option></select><input id='search'/></div>";
    $html .= "<table class='main_table sortable'>";
    $html .= "<thead>";
    $html .= "<tr class='header_row'>";
    $i = 0;
    foreach($headers as $header){
        if ($column_sort[$i] === $sort){
            $html .= "<th class='sort sorted' id='" . $column_sort[$i] . "'>" . $header . "</th>";
        } else {
            $html .= "<th class='sort' id='" . $column_sort[$i] . "'>" . $header . "</th>";
        }
        $i++;
    }
    $html .= "</tr>";

    $html .= "</thead>";
    if ($check === 0) {
        $ID = $array['ID'];
        $html .= "<tr class='data_row' id='$ID'>";
    }
    foreach($array as $key => $value){
        if ($check === 0){
            $html .= "<td id='$key'>".$value."</td>";
        } else {
            $ID = $array[$key]['ID'];
            $html .= "<tr class='data_row' id='$ID'>";
            foreach($value as  $sub_value){
                $html .= "<td>".$sub_value."</td>";
            }
            $html .= "</tr>";
        }
    }
    if ($check === 0) {
        $html .= "</tr>";
    }
    $html .= "</table>";
    return $html;
}

function options_listing ($array,$headers,$ID){
    $html = "<div class='options row'>";
    $html .= "<div class='col'>";
    $i = 0;
    foreach($array as $key => $value){
        $html .= "<div id='$key' class='row'>";
        $html .= "<h4 class='col'>".$headers[$i]."</h4>";
        $html .= "<p class='col'>".$value."</p>";
        if($_SESSION['level'] === '3') {
            $html .= "<div class='buttons'><button class='change btn btn-primary' id='$ID'>Change</button></div>";
        }
        $html .= "</div>";
        $i++;
    }
    $html .= "</div>";
    return $html;
}

function change_option($query){
    global $db;
    if(mysqli_query($db,$query)){
        return true;
    }
}
