<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/9/17
 * Time: 8:33 AM
 */


include 'var.php';
include 'connect.php';
include 'functions.php';
$function = $_POST['function'];
$info = array(
    "table"=>'',
    "function"=>'',
    "column"=>'',
    "change"=>'',
    "selector"=>'',
    "change_check"=>'',
    "check_column"=>'',
    "block"=>'',
    "data"=> array(
        "form"=> '',
        "buttons"=> '',
        "conditional_statement"=> '',
        "conditional_function"=> ''
    ),
);



//Master
if ($function === 'select_generation'){
    $selection = $_POST['selection'];
    // 'hello';
    if ($selection === 'product'){
        select_creation("SELECT * FROM Products", 'Products','product');
    } else if ($selection === 'bin'){
        select_creation("SELECT * FROM bins", 'Bins', 'bin');
    } else if ($selection === 'client'){
        select_creation("SELECT ID,name FROM clients",'Client','clients');
    }  else if ($selection === 'carrier'){
        select_creation("SELECT ID,name FROM carriers",'Carrier','carrier');
    }
}

if ($function === 'delete_sale'){
    $selection = $_POST['selection'];
    $info['table'] = 'sale';
    $info['selector'] = $selection;
    $query = "DELETE FROM " .$info['table']. " WHERE ID='" .$info['selector']. "'";
    table_query($query);
    $info['table'] = 'analysis';
    $query = "DELETE FROM " .$info['table']. " WHERE sale_id='" .$info['selector']. "'";
    table_query($query);
}

if ($function === 'main_table') {
    $table = $_POST['table'];
    $title = $_POST['title'];
    $script = $_POST['script'];
    $ID = $_POST['id'];
    $selection = $_POST['selection'];
    if($_SESSION['L'] >= 2) {
        echo "<script src='$script'></script>";
    }
    if ($table === 'clients'){echo $client_table_head;}
    else if ($table === 'login'){echo $user_table_head;}
    else if ($table === 'tickets'){echo $ticket_table_head;}
    else if ($table === 'Products'){echo $product_table_head;}
    else if ($table === 'carriers'){echo $carriers_table_head;}
    else if ($table === 'bins'){echo $bins_table_head;}
    else if ($table === 'sale'){echo $sale_table_head;}
    else if ($table === 'analysis'){echo $analysis_table_head;}
    horizontal_table("SELECT $selection FROM $table");
}

if ($function === 'create_new_modal'){
    $selection = $_POST['selection'];
    $date = date("Y-m-d");
    if ($selection === 'user'){echo $new_user_form;}
    if ($selection === 'client'){echo $new_client_form;}
    if ($selection === 'product'){echo $new_product_form;}
    if ($selection === 'bin'){echo $new_bin_form;}
    if ($selection === 'sale'){new_sale_form();}
    if ($selection === 'ticket'){
        echo "<form class='row' action='' method='post' id='new_ticket_form'>";
        echo "<div class='col'>";
        echo "<div class='row justify-center'>";
        echo "<h4 class='col'>Date:</h4>";
        echo "<input value='".$date."' class='col' type='date' title='date' name='date' id='date' /></div>";
        echo "<div class='row'><h4 class='col'>Client:</h4>";
        select_creation_new("SELECT ID,name FROM clients",'Client','clients');
        echo "</div>";
        echo "<div class='row'><h4 class='col'>Product:</h4>";
        select_creation_new("SELECT * FROM Products", 'Products','product');
        echo "</div>";
        echo "<div id='contract_container' class='row'>";
        echo "</div>";
        echo "<div id='ticket_form_container' class='col'></div>";
        echo "</div>";
        echo "</form>";
    }
}

if ($function === 'delete') {
    $info['table'] = $_POST['table'];
    $info['selector'] = $_POST['selection'];
    table_row_deletion($info);
}

if ($function === 'create') {
    $selection = $_POST['selection'];
    $info['table'] = $_POST['table'];
    if ($selection === 'user'){
        $email = mysqli_real_escape_string($db,$_POST['email']);
        $pass_word = md5(mysqli_real_escape_string($db,$_POST['password']));
        $level = intval(mysqli_real_escape_string($db,$_POST['level']));
        $f_name = mysqli_real_escape_string($db,$_POST['f_name']);
        $l_name = mysqli_real_escape_string($db,$_POST['l_name']);
        $info['column'] = 'email,password,Level,f_name,l_name';
        $info['change'] = "'$email', '$pass_word', '$level', '$f_name', '$l_name'";
        $info['change_check'] = $email;
        $info['check_column'] = 'email';
        $info['test'] = 'simple_test';
        table_row_creation($info);
    }
    if ($selection === 'product'){
        $info['change'] = "'".mysqli_real_escape_string($db,$_POST['name'])."'";
        $info['column'] = 'product_name';
        $info['test'] = 'simple_test';
        $info['change_check'] = mysqli_real_escape_string($db,$_POST['name']);
        $info['check_column'] = 'product_name';
        table_row_creation($info);
    }
    if ($selection === 'bin'){
        $info['change'] = "'".mysqli_real_escape_string($db,$_POST['location'])."'";
        $info['column'] = 'location';
        $info['test'] = 'simple_test';
        $info['change_check'] = mysqli_real_escape_string($db,$_POST['location']);
        $info['check_column'] = 'location';
        table_row_creation($info);
    }
    if ($selection === 'client'){
        $name = trim(mysqli_real_escape_string($db, $_POST['name']));
        $s_address = trim(mysqli_real_escape_string($db, $_POST['s_address']));
        $city = trim(mysqli_real_escape_string($db, $_POST['city']));
        $state = trim(mysqli_real_escape_string($db, $_POST['state']));
        $zip = trim(mysqli_real_escape_string($db, $_POST['zip']));
        $phone = trim(mysqli_real_escape_string($db, $_POST['phone']));
        $type = trim(mysqli_real_escape_string($db, $_POST['type']));
        $info['column'] = 'name,street_address,city,state,zip,phone,type';
        $info['change'] = "'$name', '$s_address', '$city', '$state', '$zip', '$phone', '$type'";
        $info['change_check'] = $name;
        $info['test'] = 'simple_test';
        $info['check_column'] = 'name';
        table_row_creation($info);
    }
    if ($selection === 'contact'){
        $client_name = trim(mysqli_real_escape_string($db,$_POST['client_name']));
        $array = database_array("SELECT ID FROM clients WHERE name='$client_name'");
        $client_id = $array['ID'];
        $name = trim(mysqli_real_escape_string($db, $_POST['cname']));
        $s_address = trim(mysqli_real_escape_string($db, $_POST['c_s_address']));
        $city = trim(mysqli_real_escape_string($db, $_POST['c_city']));
        $state = trim(mysqli_real_escape_string($db, $_POST['c_state']));
        $zip = trim(mysqli_real_escape_string($db, $_POST['c_zip']));
        $phone = trim(mysqli_real_escape_string($db, $_POST['cphone']));
        $info['column'] = 'client_id,name,s_address,city,state,zip,phone';
        $info['change'] = "'$client_id', '$name', '$s_address', '$city', '$state', '$zip', '$phone'";
        $info['change_check'] = $name;
        $info['test'] = 'simple_test';
        $info['check_column'] = 'name';
        table_row_creation($info);
    }
    if ($selection === 'ticket'){
        $date = trim(mysqli_real_escape_string($db, $_POST['date']));
        $product_id = trim(mysqli_real_escape_string($db, $_POST['Products']));
        $bin_id = trim(mysqli_real_escape_string($db, $_POST['bin']));
        $contract_id = trim(mysqli_real_escape_string($db, $_POST['contract']));
        $client_id = trim(mysqli_real_escape_string($db, $_POST['Client']));
        $protein = trim(mysqli_real_escape_string($db, $_POST['protein']));
        $test_wt= trim(mysqli_real_escape_string($db, $_POST['test_weight']));
        $fm_dockage= trim(mysqli_real_escape_string($db, $_POST['fm_dockage']));
        $plump_thin= trim(mysqli_real_escape_string($db, $_POST['plump_thin']));
        $moisture= trim(mysqli_real_escape_string($db, $_POST['moisture']));
        $vom= trim(mysqli_real_escape_string($db, $_POST['vom']));
        $cofo= trim(mysqli_real_escape_string($db, $_POST['cofo']));
        $other= trim(mysqli_real_escape_string($db, $_POST['other']));
        $bushels= trim(mysqli_real_escape_string($db, $_POST['bushels']));
        $dockage= trim(mysqli_real_escape_string($db, $_POST['dockage']));
        $comments= trim(mysqli_real_escape_string($db, $_POST['comment']));
        $info['column'] = "date,product_id,client_id,protein,moisture,test_weight,dockage,plump_thin,vom,cofo,other,dockage_price,bin_id,net_bushels,comment,contract_id";
        $info['change'] = "'$date', '$product_id', '$client_id', '$protein', '$moisture', '$test_wt', '$fm_dockage', '$plump_thin', '$vom', '$cofo', '$other', '$dockage', '$bin_id', '$bushels', '$comments', '$contract_id'";
        table_row_creation($info);
    }
    if ($selection === 'sale'){
        $date = trim(mysqli_real_escape_string($db, $_POST['date']));
        $po_number = trim(mysqli_real_escape_string($db, $_POST['po_number']));
        $d_number = trim(mysqli_real_escape_string($db, $_POST['d_number']));
        $lbs = trim(mysqli_real_escape_string($db, $_POST['lbs']));
        $bushels = trim(mysqli_real_escape_string($db, $_POST['bushels']));
        $client_id= trim(mysqli_real_escape_string($db, $_POST['client']));
        $f_paid= trim(mysqli_real_escape_string($db, $_POST['freight_paid']));
        $f_rate= trim(mysqli_real_escape_string($db, $_POST['freight_rate']));
        $seal_numbers= trim(mysqli_real_escape_string($db, $_POST['seal_numbers']));
        $product_id= trim(mysqli_real_escape_string($db, $_POST['product']));
        $carrier= trim(mysqli_real_escape_string($db, $_POST['carrier']));
        $trailer_number= trim(mysqli_real_escape_string($db, $_POST['trailer_number']));
        $truck_number= trim(mysqli_real_escape_string($db, $_POST['truck_number']));
        $bin_id= trim(mysqli_real_escape_string($db, $_POST['bin']));
        $fat= trim(mysqli_real_escape_string($db, $_POST['fat']));
        $fat_test= trim(mysqli_real_escape_string($db, $_POST['fat_test']));
        $fiber= trim(mysqli_real_escape_string($db, $_POST['fiber']));
        $fiber_test= trim(mysqli_real_escape_string($db, $_POST['fiber_test']));
        $ash= trim(mysqli_real_escape_string($db, $_POST['ash']));
        $ash_test= trim(mysqli_real_escape_string($db, $_POST['ash_test']));
        $a_toxin= trim(mysqli_real_escape_string($db, $_POST['a_toxin']));
        $a_toxin_test= trim(mysqli_real_escape_string($db, $_POST['a_toxin_test']));
        $protein= trim(mysqli_real_escape_string($db, $_POST['protein']));
        $protein_test= trim(mysqli_real_escape_string($db, $_POST['p_test']));
        $moisture= trim(mysqli_real_escape_string($db, $_POST['moisture']));
        $moisture_test= trim(mysqli_real_escape_string($db, $_POST['m_test']));
        $test_wt= trim(mysqli_real_escape_string($db, $_POST['test_wt']));
        $test_wt_test= trim(mysqli_real_escape_string($db, $_POST['t_test']));
        $vom= trim(mysqli_real_escape_string($db, $_POST['vom']));
        $vom_test= trim(mysqli_real_escape_string($db, $_POST['v_test']));
        $info['column'] = "date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id,f_paid,f_rate";
        $info['change'] = "'$date', '$po_number', '$d_number', '$lbs', '$bushels','$client_id', '$seal_numbers', '$carrier', '$trailer_number','$truck_number', '$product_id', '$f_paid', '$f_rate'";
        $info['table2'] = 'analysis';
        $info['column2'] = "bin_id,product_id,fat,fat_test,fiber,fiber_test,ash,a_test,aflatoxin,a_toxin_test,protein,p_test,moisture,m_test,test_weight,t_test,vomitoxin,v_test";
        $info['change2'] = "'$bin_id', '$product_id', '$fat','$fat_test', '$fiber', '$fiber_test', '$ash', '$ash_test', '$a_toxin', '$a_toxin_test', '$protein', '$protein_test', '$moisture', '$moisture_test', '$test_wt', '$test_wt_test', '$vom', '$vom_test'";
        table_row_creation_double($info);
    }

}
if ($function === 'analysis_modal'){

}
if ($function === 'options_modal1'){
    $ID = $_POST['selection'];
    $table = $_POST['table'];
    $query = "SELECT * FROM $table WHERE ID='$ID'";
    $heading = $_POST['heading'];
    $item = $_POST['item'];
    vertical_table($query,$heading, $ID, $item);
    if ($_SESSION['L'] === 3) {
        echo "<button id='$ID' class='btn btn-danger close delete' data-dismiss='modal'>Delete</button>";
    }
}

if ($function === 'sale_options') {
    if ($_POST['selection'] === 'sale_relation'){
        $analysis_id = $_POST['id'];
        $ID_select = mysqli_fetch_assoc(mysqli_query($db,"SELECT id FROM sale WHERE id='$analysis_id'"));
        $ID = $ID_select['id'];
    }else {
        $ID = $_POST['id'];
    }
    $query = "SELECT * FROM sale WHERE id='".$ID."'";
    $heading = array('ID', 'Date', 'Purchase Order', 'Delivery No', 'LBS', 'Bushels', 'Client', 'Seal Number', 'Carrier', 'Trailer No', 'Truck No', 'Product', 'Freight Paid By', 'Freight Rate');
    $item = array('id', 'date', 'PO', 'deliver_no', 'pounds', 'bushels', 'client_id', 'seal_number', 'carrier_id', 'trailer', 'truck', 'product_id', 'f_paid', 'f_rate');
    $func = 'edit_sale_item';
    vertical_table($query,$heading, $ID, $item);
    if ($_SESSION['L'] > 2) {
        echo "<button id='$ID' class='btn btn-danger delete' data-dismiss='modal' >Delete</button>";
    }
}

if ($function === 'products_options') {
    $ID = $_POST['id'];
    $query = "SELECT * FROM Products WHERE ID='" . $ID . "'";
    $heading = array('Product ID', 'Product Name');
    $item = array('ID', 'product_name');
    $func = 'edit_product_item';
    vertical_table($query, $heading, $ID, $item);
    if ($_SESSION['L'] > 2) {
        echo "<div class='center-block'><button id='$ID' class='btn btn-danger delete' data-dismiss='modal'>Delete</button></div>";
    }
}

if ($function === 'analysis_options') {
    if ($_POST['selection'] === 'analysis_relation'){
        $sale_id = $_POST['id'];
        $ID_select = mysqli_fetch_assoc(mysqli_query($db,"SELECT ID FROM analysis WHERE sale_id='$sale_id'"));
        $ID = $ID_select['ID'];
    }else {
        $ID = $_POST['id'];
    }
    $query = "SELECT * FROM analysis WHERE id='".$ID."'";
    $heading = array('ID', 'Bin', 'Bill of Lading', 'Product', 'Fat (%)', 'Fat Test Frequency', 'Fiber (%)', 'Fiber Test Frequency', 'Ash (%)', 'Ash Test Frequency', 'Aflatoxin (ppm)', 'Aflatoxin Test Frequency', 'Protein (%)', 'Protein Test Frequency', 'Moisture (%)', 'Moisture Test Frequncy', 'Test Weight', 'Test Weight Frequency', 'Vomitoxin (ppm)', 'Vomitoxin Test Frequency');
    $item = array('ID', 'bin_id', 'sale_id', 'product_id', 'fat', 'fat_test', 'fiber', 'fiber_test', 'ash', 'a_test', 'aflatoxin', 'a_toxin_test', 'protein', 'p_test', 'moisture', 'm_test', 'test_weight', 't_test', 'vomitoxin', 'v_test');
    $func = 'edit_analysis_item';
    vertical_table($query,$heading, $ID, $item);
}

//
//  Login
//

if ($function === 'login'){
    $user_name = mysqli_real_escape_string($db,$_POST['username']);
    $pass_word = md5(mysqli_real_escape_string($db,$_POST['password']));
    login($user_name,$pass_word);
}

if ($function === 'logout') {session_destroy (); echo 'true';}
//
//  User Management
//

if ($function === 'change_level'){
    $info['selector'] = $_POST['selection'];
    $info['change'] = $_POST['change'];
    $info['table'] = 'login';
    $info['column'] = 'Level';
    table_change_check($info);
}

if ($function === 'change_password') {
    $info['selector'] = $_POST['selection'];
    $info['change'] = md5($_POST['change']);
    $info['table'] = 'login';
    $info['column'] = 'password';
    table_change_check($info);
}

if ($function === 'change_email') {
    $info['selector'] = $_POST['selection'];
    $info['change'] = $_POST['change'];
    $info['table'] = 'login';
    $info['column'] = 'email';
    table_change_check($info);
}

if ($function === 'options_modal'){
    $ID = $_POST['id'];
    $query = "SELECT * FROM login WHERE ID='".$ID."'";
    $heading = array('ID', 'Email', 'First Name', 'Last Name', 'Level', 'Password');
    $item = array('ID', 'email', 'f_name', 'l_name', 'Level', 'password');
    $func = 'edit_user_item';
    vertical_table($query,$heading,$ID,$item);
    if ($_SESSION['L'] > 2) {
        echo "<div class='justify-center'><button id='$ID' class='btn btn-danger center-block' data-dismiss='modal' >Delete</button></div>";
    }
}

//
//  Client Management
//

if ($function === 'client_state_form'){echo $state_select;}

if ($function === 'change_client_name'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_name($ID,$change,$db);}

if ($function === 'change_client_address'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_address($ID,$change,$db);}

if ($function === 'change_client_state'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_state($ID,$change,$db);}

if ($function === 'change_client_city'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_city($ID,$change,$db);}

if ($function === 'change_client_zip'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_zip($ID,$change,$db);}

if ($function === 'change_client_phone'){$ID = $_POST['selection'];$change = $_POST['change'];change_client_phone($ID,$change,$db);}

if ($function === 'edit_client_modal'){
    $ID = $_POST['selection'];
    $query = "SELECT * FROM clients WHERE ID='".$ID."'";
    $heading = array('ID', 'Name', 'Street Address', 'City', 'State', 'Zip', 'Phone', 'Type');
    $item = array('ID', 'name', 'street_address', 'city', 'state', 'zip', 'phone', 'type');
    $func = 'edit_client_item';
    vertical_table($query,$heading,$ID,$item);
}

//
//  Ticket Management
//

if ($function === 'ticket_measurements'){$ID = $_POST['selection'];measurement_modal($ID);}
if ($function === 'analysis_measurement'){
    $ID = $_POST['selection'];
        $rows = database_array("SELECT bin_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin FROM analysis WHERE sale_id='".$ID."'");
        echo <<<HTMLBLOCK
        <table class='table table-bordered'><tr>
        <th>Bin</th>
        <th>Product</th>
        <th>Fat</th>
        <th>Fiber</th>
        <th>Ash</th>
        <th>Aflatoxin</th>
        <th>Protein</th>
        <th>Moisture</th>
        <th>Test Weight</th>
        <th>Vomitoxin</th>
        </tr><tr>
HTMLBLOCK;

        foreach ($rows as $key => $row){
            if ($key === 'bin_id'){
                $bin = mysqli_fetch_assoc(mysqli_query($db,"SELECT location FROM bins WHERE ID='$row'"));
                echo "<td>".$bin['location']."</td>";
            } else if ($key === 'product_id'){
                $bin = mysqli_fetch_assoc(mysqli_query($db,"SELECT product_name FROM Products WHERE ID='$row'"));
                echo "<td>".$bin['product_name']."</td>";
            } else {
                echo "<td>$row</td>";
            }

        }
        echo "</tr></table>";

    }

if ($function === 'carrier_options') {
    $ID = $_POST['id'];
    $query = "SELECT * FROM carriers WHERE ID='".$ID."'";
    $heading = array('Carrier ID', 'Name', 'Street Address', 'City', 'State', 'Zip', 'Phone');
    $item = array('ID', 'name', 's_address', 'city', 'state', 'zip', 'phone');
    $func = 'edit_carrier_item';
    vertical_table($query,$heading, $ID, $item);
    if ($_SESSION['L'] === 3) {
        echo "<div class='center-block'><button class='btn btn-danger close' data-dismiss='modal'>Delete</button></div>";
    }
}

if ($function === 'bin_options') {
    $ID = $_POST['id'];
    $query = "SELECT * FROM bins WHERE ID='".$ID."'";
    $heading = array('Bin ID', 'Location');
    $item = array('ID', 'location');
    $func = 'edit_bin_item';
    vertical_table($query,$heading, $ID, $item);
    if ($_SESSION['L'] === 3) {
        echo "<div class='center-block'><button class='btn btn-danger close' data-dismiss='modal'>Delete</button></div>";
    }
}

if ($function === 'ticket_options') {
    $ID = $_POST['id'];
    $query = "SELECT * FROM tickets WHERE ID='".$ID."'";
    $heading = array('ID', 'Date', 'Product', 'Client', 'Protein', 'Moisture', 'Test Weight', 'FM/Dockage', 'Plump Thin', 'Vomitoxin', 'COFO', 'Total Dockage', 'Bin', 'Net Bushels', 'Comments', 'Other');
    $item = array('ID', 'date', 'product', 'client', 'protein', 'moisture', 'test_wt', 'fm_dockage', 'plump', 'vom', 'cofo', 'dockage', 'bin', 'Net bushels', 'comment', 'other');
    $func = 'edit_ticket_item';
    vertical_table($query,$heading, $ID, $item);
    if ($_SESSION['L'] === 3) {
        echo "<div class='center-block'><button onClick='delete_ticket($ID);' class='btn btn-danger close' data-dismiss='modal' >Delete</button></div>";
    }
}

if ($function === 'update_table'){
    $info['table'] = $_POST['table'];
    $info['selector'] = $_POST['selection'];
    $info['column'] = $_POST['column'];
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['function'] = $_POST['test'];
    $info['check_column'] = $_POST['column'];
    $info['change_check'] = $_POST['change'];
    table_change_check($info);
}

if ($function === 'edit_ticket_form_date'){
    table_change_check($info);
    $info['selector'] = $_POST['selection'];
    $date_raw = $_POST['current_value'];
    $date_var = strtotime($date_raw);
    $date = date('Y-m-d',$date_var);
    $info['data']['form'] = "<input id='new_ticket_item' type='date' value='$date' class='center-block'>";
}

if ($function === 'edit_ticket_form_product'){
    $ID = $_POST['selection'];
    $product = $_POST['current_value'];
    $query = "SELECT * FROM Products";
    $result = mysqli_query($db,$query);
    echo "<select id='new_ticket_item' class='center-block'>";
    while ($row = mysqli_fetch_assoc($result)){
        if ($row['product_name'] == $product){
            echo "<option value=".$row['ID']." selected>".$row['product_name']."</option>";
        } else {
            echo "<option value=".$row['ID'].">".$row['product_name']."</option>";
        }
    }
    echo "</select>";
}

if ($function === 'edit_ticket_form_client'){
    $ID = $_POST['selection'];
    $product = $_POST['current_value'];
    $query = "SELECT * FROM clients";
    $result = mysqli_query($db,$query);
    echo "<select id='new_ticket_item' se class='center-block'>";
    while ($row = mysqli_fetch_assoc($result)){
        if ($row['name'] == $product){
            echo "<option value=".$row['ID']." selected>".$row['name']."</option>";
        } else {
            echo "<option value=".$row['ID'].">".$row['name']."</option>";
        }
    }
    echo "</select>";
}

if ($function === 'edit_ticket_form_bin'){
    $ID = $_POST['selection'];
    $product = $_POST['current_value'];
    $query = "SELECT * FROM bins";
    $result = mysqli_query($db,$query);
    echo "<select id='new_ticket_item' class='center-block'>";
    while ($row = mysqli_fetch_assoc($result)){
        if ($row['location'] == $product){
            echo "<option value=".$row['ID']." selected>".$row['location']."</option>";
        } else {
            echo "<option value=".$row['ID'].">".$row['location']."</option>";
        }
    }
    echo "</select>";
}

if ($function === 'edit_ticket_form_protein'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_moisture'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_test_wt'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_fm_dockage'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_plump'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_vom'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_cofo'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'edit_ticket_form_dockage'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}
if ($function === 'edit_ticket_form_bushels'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}
if ($function === 'edit_ticket_form_comment'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}
if ($function === 'edit_ticket_form_other'){
    $protein = $_POST['current_value'];
    echo "<input id='new_ticket_item' value='$protein' class='center-block'>";
}

if ($function === 'update_ticket_date'){
    $info['table'] = 'tickets';
    $info['column'] = 'date';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}

if ($function === 'update_ticket_product'){
    $info['table'] = 'tickets';
    $info['column'] = 'product_id';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}

if ($function === 'update_ticket_client'){
    $info['table'] = 'tickets';
    $info['column'] = 'client_id';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}

if ($function === 'update_ticket_protein'){
    $info['table'] = 'tickets';
    $info['column'] = 'protein';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_moisture'){
    $info['table'] = 'tickets';
    $info['column'] = 'moisture';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_test_wt'){
    $info['table'] = 'tickets';
    $info['column'] = 'test_weight';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_product'){
    $info['table'] = 'tickets';
    $info['column'] = 'client_id';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_fm_dockage'){
    $info['table'] = 'tickets';
    $info['column'] = 'dockage';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_plump'){
    $info['table'] = 'tickets';
    $info['column'] = 'plump_thin';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_vom'){
    $info['table'] = 'tickets';
    $info['column'] = 'vom';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_cofo'){
    $info['table'] = 'tickets';
    $info['column'] = 'cofo';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_dockage'){
    $info['table'] = 'tickets';
    $info['column'] = 'dockage_price';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_bin'){
    $info['table'] = 'tickets';
    $info['column'] = 'bin_id';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_bushels'){
    $info['table'] = 'tickets';
    $info['column'] = 'net_bushels';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_comment'){
    $info['table'] = 'tickets';
    $info['column'] = 'comment';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}
if ($function === 'update_ticket_other'){
    $info['table'] = 'tickets';
    $info['column'] = 'other';
    $info['change'] = $_POST['change'];
    $info['function'] = '';
    $info['selector'] = $_POST['selection'];
    table_change_check($info);
}

if ($function === 'ticket_contract'){
    $client = $_POST['client'];
    $product = $_POST['product'];
    $query = "SELECT ID,contract_no FROM contracts WHERE client_id='$client' AND product_id='$product'";

    if (simple_check_query($query) === 0){?>
        <h4 class='col'>No Contracts Available:</h4>
        <a class='btn btn-info' id='new_contract_ticket'>Make A Contract</a>
        <script src="../js/contract_management/main.js"></script>
        <script>
            $("#new_contract_ticket").click(function(){
                $('#my_modal').modal('toggle');
            });

        </script>
    <?php } else{
        $data = whole_table_array($query);
    ?>
        <h4 class='col'>Contract:</h4>
        <select class='col' name='contract' id='contract' title='contract'><option>--Please Select--</option>
        <?php foreach ($data as $row) { ?>
            <option value='<?=$row[0]?>'>Contract #<?=$row[0]?></option>
        <?php } ?>
        </select>
        <script>
            $("#contract").change(function (){
                var success = function(response){
                    $("#ticket_form_container").html(response);
                };
                data = "function=ticket_form";
                ajax(data, success, '/php/tickets/new_form.php');
            });
        </script>
    <?php }

}
if ($function === 'show_pdf'){
  $ID = $_POST['ID'];
  pdf_show($ID);
}
