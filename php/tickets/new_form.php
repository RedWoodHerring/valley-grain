<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/8/17
 * Time: 8:57 AM
 */
include '../functions.php';
require_once '../blocks.php';
$query = "SELECT ID,location FROM bins";
$row = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
if ($_POST['function'] === 'ticket_form'){?>
    <div class="row">
        <h4 class='col'>Bin:</h4>
        <select class="col justify-center" title="bin" name="bin" id="bin">
            <?php foreach($row as $value){?>
                <option value="<?=$value['ID']?>"><?=$value['location']?></option>
            <?php } ?>
        </select>
    </div>
    <div class="row">
        <h4 class='col'>Protein:</h4>
        <input class="col justify-center" title="protein" name="protein" id="protein" />
    </div>
    <div class="row">
        <h4 class='col'>Moisture:</h4>
        <input class="col" title="moisture" name="moisture" id="moisture" />
    </div>
    <div class="row">
        <h4 class='col'>Test Weight:</h4>
        <input class="col" title="test_weight" name="test_weight" id="test_weight" />
    </div>
    <div class="row">
        <h4 class='col'>FM/Dockage:</h4>
        <input class="col" title="fm_dockage" name="fm_dockage" id="fm_dockage" />
    </div>
    <div class="row">
        <h4 class='col'>Plump Thin:</h4>
        <input class="col" title="plump_thin" name="plump_thin" id="plump_thin" />
    </div>
    <div class="row">
        <h4 class='col'>VOM:</h4>
        <input class="col" title="vom" name="vom" id="vom" />
    </div>
    <div class="row">
        <h4 class='col'>COFO:</h4>
        <input class="col" title="cofo" name="cofo" id="cofo" />
    </div>
    <div class="row">
        <h4 class='col'>Other:</h4>
        <input class="col" title="other" name="other" id="other" />
    </div>
    <div class="row">
        <h4 class='col'>Bushels:</h4>
        <input class="col" title="bushels" type="number" id="bushels" name="bushels"/>
    </div>
    <div class="row">
        <h4 class='col'>Dockage Price:</h4>
        <input class="col" title="dockage" type="number" id="dockage" name="dockage"/>
    </div>
    <div class="row">
        <h4 class='col'>Comment:</h4>
        <textarea class="col" title="comment" id="comment" name="comment"></textarea>
    </div>
    <script>
        $('#submit_new_ticket').click(function (){
        if ($('input').val() === null || $('select').val() === null) {
        err.html("<p class='alert bg-danger'>Please Fill the Entire Form");
            } else {
            data = $("#new_ticket_form").serialize()+'&function=create&selection=ticket&table=tickets';
            func_yes = function () {
            $('#modal_msg').html("<p class='alert bg-success'>ticket Added</p><button id='new_ticket_button' class='btn btn-default justify-center'>Create New Ticket</button>");
        $('#modal_footer').html('');
        };
        func_no = function (response) {
        $('#modal_msg').html("<p>Ticket cannot be added, please contact an admin</p>");
        console.log(response);
        };
        ajax_send_data(data,func_yes,func_no);
        selection = 'ID,date,product_id,client_id,dockage_price,bin_id,net_bushels';
        data = 'selection='+selection+'&function=main_table&table=tickets&title=Create New Ticket&script=/js/ticket_management/main.js&id=new_ticket_button';
        ajax_generation(data,data_table_reload);
        }
        });
    </script>
<?php } ?>