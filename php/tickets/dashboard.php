<?php

require "../functions.php";
$ID = $_POST['ID'];
$array = database_array("SELECT * FROM tickets WHERE ID='$ID'");
$product_array = database_array("SELECT * FROM Products WHERE ID='".$array['product_id']."'");
$client_array = database_array("SELECT * FROM clients WHERE ID='".$array['client_id']."'");
$contract_array = database_array("SELECT * FROM contracts WHERE ID='".$array['contract_id']."'");
$array['product_name'] = $product_array['product_name'];
$array['client_name'] = $client_array['name'];
$array['client_address'] = $client_array['s_address'];
$array['client_city'] = $client_array['city'];
$array['client_state'] = $client_array['state'];
$array['client_phone'] = $client_array['phone'];
echo "<div id='data_container'>";
echo "<div id='left-column'>";
echo "<div id='ticket_info'>";
$info_columns = array('date','product_name');
$headings = array('Date:','Product:');
$i = 0;
foreach($info_columns as $value){
  echo "<div>";
  echo "<h5>$headings[$i]</h5>";
  echo "<h7>".$array[$value]."</h7>";
  echo "</div>";
  $i++;
}
echo "</div>";
echo "<div id='client_info'>";
echo "<h4>Client</h4>";
$info_columns = array('client_name','client_address','client_city','client_state','client_phone');
$headings = array('Name:','Address:','City:','State:','Phone:');
$i = 0;
foreach($info_columns as $value){
  echo "<div>";
  echo "<h5>$headings[$i]</h5>";
  echo "<h7>".$array[$value]."</h7>";
  echo "</div>";
  $i++;
}
echo "</div>";
echo "<div id='analysis_container'>";
echo "<h4>Analysis</h4>";
echo "<table class='table table-bordered table-striped'>";
echo "<thead>";
echo "<tr>";
echo "<th>Properties</th>";
echo "<th>Value</th>";
echo "</tr>";
echo "</thead>";
echo "</tbody>";
$info_columns = array('protein','moisture','test-weight','dockage','plump_thin','vom','cofo');
$headings = array('Protein (%)','Moisture (%)','Test Weight','FM/Dockage','Plump-Thin','VOM (ppm)','COFO (odor)');
$i = 0;
foreach($info_columns as $value){
  echo "<tr>";
  echo "<td>$headings[$i]</td>";
  echo "<td>".$array[$value]."</td>";
  echo "</tr>";
  $i++;
}
echo "</tbody>";
echo "</table>";
echo "</div>";
echo "</div>";
echo "<div id='right_column'>";
echo "<div id='contract_container'>";
echo "<h4>Contract Info</h4>";
$info_columns = array('client_name','client_address','client_city','client_state','client_phone');
$headings = array('Name:','Address:','City:','State:','Phone:');
$i = 0;
foreach($info_columns as $value){
  echo "<div>";
  echo "<h5>$headings[$i]</h5>";
  echo "<h7>".$array[$value]."</h7>";
  echo "</div>";
  $i++;
}
echo "</div>";

echo "</div>";
echo "</div>";
echo "<div id='comment_container'>";
echo "</div>";
