<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/21/17
 * Time: 8:39 AM
 */

//
//  Tickets
//

//Measurement Modal
function measurement_modal($ID){
    $rows = database_array("SELECT protein, moisture, test_weight, dockage, plump_thin, vom, cofo FROM tickets WHERE ID='".$ID."'");
    echo <<<HTMLBLOCK
        <table class='table table-bordered'><tr>
        <th>Protein</th>
        <th>Moisture</th>
        <th>Test Weight</th>
        <th>Dockage</th>
        <th>Plump - Thin</th>
        <th>VOM</th>
        <th>COFO</th>
        </tr><tr>
HTMLBLOCK;

    foreach ($rows as $row){
        echo "<td>$row</td>";

    }
    echo "</tr></table>";

}

//New Ticket Modal
function new_ticket_modal ($form_1,$form_2) {
    echo $form_1;
    select_creation("SELECT ID,name FROM clients",'Client','clients');
    select_creation("SELECT * FROM Products", 'Products','product');
    select_creation("SELECT * FROM bins", 'Bins', 'bin');
    echo $form_2;
};