<?php
function new_sale_form()
{
    echo "<form class='row' id='bill_of_lading'>";
    echo "<div class='col'>";

    //DATE
    $date = date("Y-m-d");
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Date</p>";
    echo "<input class='col' type='date' name='date' value='$date'>";
    echo "</div>";

    //PO NO
    echo "<div class='row justify-center'>";
    echo "<p class='col'>PO Number</p>";
    echo "<input class='col' name='po_number'>";
    echo "</div>";

    //Delivery NO
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Delivery Number</p>";
    echo "<input class='col' name='d_number'>";
    echo "</div>";

    //Lbs
    echo "<div class='row justify-center'>";
    echo "<p class='col'>LBS</p>";
    echo "<input class='col' name='lbs'>";
    echo "</div>";

    //Bushels
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Bushels</p>";
    echo "<input class='col' name='bushels'>";
    echo "</div>";

    //Client
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Client</p>";
    select_creation("SELECT ID,name FROM clients", 'client');
    echo "</div>";

    //Freight Paid By
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Freight Paid By</p>";
    echo "<input class='col' name='freight_paid'>";
    echo "</div>";

    //Freight Rate
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Freight Rate</p>";
    echo "<input class='col' name='freight_rate'>";
    echo "</div>";

    //Seal Numbers
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Seal Numbers</p>";
    echo "<input class='col' name='seal_numbers'>";
    echo "</div>";

    //Product
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Product</p>";
    select_creation("SELECT ID,product_name FROM Products", 'product');
    echo "</div>";
    echo "<hr>";

    //Analysis
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Bin</p>";
    select_creation("SELECT ID,location FROM bins", 'bin');
    echo "</div>";
    echo "<hr>";

    echo "<div class='row justify-center'><h3>Certificate of Analysis</h3></div>";
    echo "<div class='row justify-center'>";
    echo "<p class='col justify-center'>Chemical Analysis</p>";
    echo "<p class='col justify-center'>value</p>";
    echo "<p class='col justify-center'>Test Frequency</p>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Protein (%)</p>";
    echo "<input class='col' name='protein'>";
    echo "<select class='col' name='p_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Moisture (%)</p>";
    echo "<input class='col' name='moisture'>";
    echo "<select class='col' name='m_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Test Weight</p>";
    echo "<input class='col' name='test_wt'>";
    echo "<select class='col' name='t_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Vomitoxin (ppm)</p>";
    echo "<input class='col' name='vom'>";
    echo "<select class='col' name='v_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Fat (%)</p>";
    echo "<input class='col' name='fat'>";
    echo "<select class='col' name='fat_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Fiber (%)</p>";
    echo "<input class='col' name='fiber'>";
    echo "<select class='col' name='fiber_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Ash (%)</p>";
    echo "<input class='col' name='ash'>";
    echo "<select class='col' name='ash_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";

    echo "<div class='row justify-center'>";
    echo "<p class='col'>Aflatoxin (ppb)</p>";
    echo "<input class='col' name='a_toxin'>";
    echo "<select class='col' name='a_toxin_test'><option value='L'>Each Lot</option><option value='Q'>Each Quarter</option></select>";
    echo "</div>";
    echo "<hr>";

    //Carrier
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Carrier</p>";
    select_creation("SELECT ID,name FROM carriers", 'carrier');
    echo "</div>";

    //Trailer No
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Trailer Number</p>";
    echo "<input class='col' name='trailer_number'>";
    echo "</div>";

    //Truck
    echo "<div class='row justify-center'>";
    echo "<p class='col'>Truck Number</p>";
    echo "<input class='col' name='truck_number'>";
    echo "</div>";
}