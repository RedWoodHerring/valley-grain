<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/21/17
 * Time: 8:38 AM
 */

//
//  Client Manipulation
//


//Edit Client Information
function edit_client_modal ($db,$ID){
    $query = "SELECT * FROM clients WHERE ID='$ID'";
    $result = mysqli_query($db,$query);
    while($row = mysqli_fetch_assoc($result)):
        echo "<table class='table'><tbody>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>Name:</h4></th>";
        echo "<td id='name_container'><h4 id='name_value'>".$row['name']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_name edit'>Edit</button></td>";
        echo "</tr>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>Street Address:</h4></th>";
        echo "<td id='address_container'><h4 id='address_value'>".$row['street_address']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_address edit'>Edit</button></td>";
        echo "</tr>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>City:</h4></th>";
        echo "<td id='city_container'><h4 id='city_value'>".$row['city']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_city edit'>Edit</button></td>";
        echo "</tr>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>State:</h4></th>";
        echo "<td id='state_container'><h4 id='state_value'>".$row['state']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_state edit'>Edit</button></td>";
        echo "</tr>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>Zip Code:</h4></th>";
        echo "<td id='zip_container'><h4 id='zip_value'>".$row['zip']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_zip edit'>Edit</button>";
        echo "</td>";
        echo "</tr>";
        echo "<tr class='client_edit_row'>";
        echo "<th><h4>Phone:</h4></th>";
        echo "<td id='phone_container'><h4 id='phone_value'>".$row['phone']."</h4></td>";
        echo "<td><button id='$ID' class='btn btn-sm col edit_phone edit'>Edit</button></td>";
        echo "</tr>";
    endwhile;
    if ($_SESSION['L'] == 3) {
        echo "<tr><td><button id='$ID' class='btn btn-danger delete_client justify-center' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>Delete</span></button></td></tr>";
    }
    echo "</tbody></table>";

}

//Change Client Name
function change_client_name ($ID,$change,$db){
    $query_check = "SELECT name FROM clients WHERE name='$change'";
    $query = "UPDATE clients SET name='$change' WHERE ID='$ID'";
    $result_check = mysqli_query($db,$query_check);
    $row_check = mysqli_num_rows($result_check);
    if ($row_check > 0){
        echo "Name Taken";
    } else {
        if (mysqli_query($db, $query)) {
            echo "true";
        }
    }
}

function change_client_address ($ID,$change,$db){
    $query = "UPDATE clients SET street_address='$change' WHERE ID='$ID'";
    if (mysqli_query($db, $query)) {
        echo "true";
    }
}

function change_client_city ($ID,$change,$db){
    $query = "UPDATE clients SET city='$change' WHERE ID='$ID'";
    if (mysqli_query($db, $query)) {
        echo "true";
    }
}

function change_client_state ($ID,$change,$db){
    $query = "UPDATE clients SET state='$change' WHERE ID='$ID'";
    if (mysqli_query($db, $query)) {
        echo "true";
    }
}

function change_client_zip ($ID,$change,$db){
    $query = "UPDATE clients SET zip='$change' WHERE ID='$ID'";
    if (mysqli_query($db, $query)) {
        echo "true";
    }
}

function change_client_phone ($ID,$change,$db){
    $query = "UPDATE clients SET phone='$change' WHERE ID='$ID'";
    if (mysqli_query($db, $query)) {
        echo "true";
    }
}