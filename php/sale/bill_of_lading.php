<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/31/17
 * Time: 8:17 AM
 */

require_once '../connect.php';
include '../functions.php';

$div_row = "<div class='row'>";
$div_col = "<div class='col'>";
$end_div = '</div>';
$hr = '<hr>';

$ID = $_POST['ID'];
$row_sale = database_array("SELECT * FROM sale WHERE id='$ID'");
$row_analysis = database_array("SELECT * FROM analysis WHERE sale_id='$ID'");
$product_id = $row_sale['product_id'];
$row_product = database_array("SELECT * FROM Products WHERE ID='$product_id'");
$carrier_id = $row_sale['carrier_id'];
$row_carrier = database_array("SELECT * FROM carriers WHERE id='$carrier_id'");
$client_id = $row_sale['client_id'];
$row_client = database_array("SELECT * FROM clients WHERE ID='$client_id'");
$bin_id = $row_analysis['bin_id'];
$row_bin = database_array("SELECT * FROM bins WHERE ID='$bin_id'");

//column 1 container
echo "<div id='bill_of_lading_view' class='row'>".$div_col;
//section 1 container
echo $div_row;
echo $div_col;
//rows
echo $div_row;
echo $div_col.'<h5>DATE:</h5>'.$end_div;
echo $div_col.$row_sale['date'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>PO No:</h5>'.$end_div;
echo $div_col.$row_sale['PO'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Delivery No:</h5>'.$end_div;
echo $div_col.$row_sale['deliver_no'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Net LBS:</h5>'.$end_div;
echo $div_col.$row_sale['pounds'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Tons/Bushels:</h5>'.$end_div;
echo $div_col.$row_sale['bushels'].$end_div;
echo $end_div;
echo $hr;
//end Rows
echo $end_div;
echo $end_div;

//section 2 container
echo $div_row;
echo $div_col;
//rows
echo $div_row;
echo $div_col.'<h5>Sold To:</h5>'.$end_div;
echo $div_col.$row_client['name'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Address:</h5>'.$end_div;
echo $div_col.$row_client['street_address'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>City/State:</h5>'.$end_div;
echo $div_col.$row_client['city'].', '.$row_client['state'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Freight To Be Paid By:</h5>'.$end_div;
echo $div_col.$row_sale['f_paid'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Freight Rate:</h5>'.$end_div;
echo $div_col.$row_sale['f_rate'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Seal Numbers:</h5>'.$end_div;
echo $div_col.$row_sale['seal_number'].$end_div;
echo $end_div;
echo $hr;
//end Rows
echo $end_div;
echo $end_div;
//end Column
echo $end_div;

//Column 2 Container
echo $div_col;
//section 1 container
echo $div_row;
echo $div_col;
//rows
echo $div_row;
echo $div_col.'<h5>Product:</h5>'.$end_div;
echo $div_col.$row_product['product_name'].$end_div;
echo $end_div;
echo $hr;
//end Rows
echo $end_div;
echo $end_div;

//section 2 container
echo $div_row;
echo "<div class='col center-block'>";

//row header
echo $div_row;
echo $div_col.'<h4 class="justify-center">Certificate of Analysis</h4>'.$end_div;
echo $end_div;

echo $div_row;

//Certificate Of Analysis Table
echo '<table class="table-bordered table center-block">';
echo '<thead>';
echo '<tr>';
echo '<th class="text-center">Chemical Analysis</th>';
echo '<th class="text-center">Value</th>';
echo '<th class="text-center">Test Frequency</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';

//row
echo '<tr>';
echo '<th>Protein (%)</th>';
echo '<td>'.$row_analysis['protein'].'</td>';
echo '<td>'.$row_analysis['p_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Moisture (%)</th>';
echo '<td>'.$row_analysis['moisture'].'</td>';
echo '<td>'.$row_analysis['m_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Test Weight</th>';
echo '<td>'.$row_analysis['test_weight'].'</td>';
echo '<td>'.$row_analysis['t_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Vomitoxin (ppm)</th>';
echo '<td>'.$row_analysis['vomitoxin'].'</td>';
echo '<td>'.$row_analysis['v_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Fat (%)</th>';
echo '<td>'.$row_analysis['fat'].'</td>';
echo '<td>'.$row_analysis['fat_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Fiber (%)</th>';
echo '<td>'.$row_analysis['fiber'].'</td>';
echo '<td>'.$row_analysis['fiber_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Ash (%)</th>';
echo '<td>'.$row_analysis['ash'].'</td>';
echo '<td>'.$row_analysis['a_test'].'</td>';
echo '</tr>';

//row
echo '<tr>';
echo '<th>Aflatoxin (ppm)</th>';
echo '<td>'.$row_analysis['aflatoxin'].'</td>';
echo '<td>'.$row_analysis['a_toxin_test'].'</td>';
echo '</tr>';

echo '</tbody></table>';
echo $end_div;
echo $end_div;
echo $end_div;


//section 3 container
echo $div_row;
echo $div_col;

//rows
echo $div_row;
echo $div_col.'<h5>Carrier:</h5>'.$end_div;
echo $div_col.$row_carrier['name'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Address:</h5>'.$end_div;
echo $div_col.$row_carrier['s_address'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>City/State:</h5>'.$end_div;
echo $div_col.$row_carrier['city'].', '.$row_carrier['state'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Phone:</h5>'.$end_div;
echo $div_col.$row_carrier['phone'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Trailer/ Unit No:</h5>'.$end_div;
echo $div_col.$row_sale['trailer'].$end_div;
echo $end_div;
echo $hr;

echo $div_row;
echo $div_col.'<h5>Truck/ Unit No:</h5>'.$end_div;
echo $div_col.$row_sale['truck'].$end_div;
echo $end_div;

//end section 3
echo $end_div;
echo $end_div;

//end Column
echo $end_div;

//end Container
echo $end_div;