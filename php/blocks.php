<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/14/17
 * Time: 11:32 AM
 */

//
//  HTML Blocks
//
$state_select = <<<HTMLBLOCK
    <option value="ND">North Dakota</option>
    <option value="AL">Alabama</option>
    <option value="AK">Alaska</option>
    <option value="AZ">Arizona</option>
    <option value="AR">Arkansas</option>
    <option value="CA">California</option>
    <option value="CO">Colorado</option>
    <option value="CT">Connecticut</option>
    <option value="DE">Delaware</option>
    <option value="FL">Florida</option>
    <option value="GA">Georgia</option>
    <option value="HI">Hawaii</option>
    <option value="ID">Idaho</option>
    <option value="IL">Illinois</option>
    <option value="IN">Indiana</option>
    <option value="IA">Iowa</option>
    <option value="KS">Kansas</option>
    <option value="KY">Kentucky</option>
    <option value="LA">Louisiana</option>
    <option value="ME">Maine</option>
    <option value="MD">Maryland</option>
    <option value="MA">Massachusetts</option>
    <option value="MI">Michigan</option>
    <option value="MN">Minnesota</option>
    <option value="MS">Mississippi</option>
    <option value="MO">Missouri</option>
    <option value="MT">Montana</option>
    <option value="NE">Nebraska</option>
    <option value="NV">Nevada</option>
    <option value="NH">New Hampshire</option>
    <option value="NJ">New Jersey</option>
    <option value="NM">New Mexico</option>
    <option value="NY">New York</option>
    <option value="NC">North Carolina</option>
    <option value="ND">North Dakota</option>
    <option value="OH">Ohio</option>
    <option value="OK">Oklahoma</option>
    <option value="OR">Oregon</option>
    <option value="PA">Pennsylvania</option>
    <option value="RI">Rhode Island</option>
    <option value="SC">South Carolina</option>
    <option value="SD">South Dakota</option>
    <option value="TN">Tennessee</option>
    <option value="TX">Texas</option>
    <option value="UT">Utah</option>
    <option value="VT">Vermont</option>
    <option value="VA">Virginia</option>
    <option value="WA">Washington</option>
    <option value="WV">West Virginia</option>
    <option value="WI">Wisconsin</option>
    <option value="WY">Wyoming</option>
HTMLBLOCK;

//New User Form
$new_user_form = <<<HTMLBLOCK
<form class="form-group col center-block" action="" method="post" id="new_user_form">
    <div class="row">
        <h5 class="col">Email</h5>
        <input class="col" type="email" title="username" name="email" id="email" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">First Name</h5>
        <input class="col" title="username" name="f_name" id="f_name" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Last Name</h5>
        <input class="col" title="username" name="l_name" id="l_name" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Password</h5>
        <input type="password" class="col" title="password" name="password" id="password" />
    </div>
    <hr>
    <div class="row">
        <h5 class="col">Level</h5>
        <select class="col" name="level" id="level">
                <option value="1">Viewer</option>
                <option value="2">Publisher</option>
                <option value="3">Administrator</option>
            </select>
    </div>
    <hr>
</form>
<div id="err_form"></div>
HTMLBLOCK;

//New Ticket Form
$new_ticket_form = <<<HTMLBLOCK
    <div class="row">
        <h4 class='col'>Protein:</h4>
        <input class="col justify-center" title="protein" name="protein" id="protein" />
    </div>
    <div class="row">
        <h4 class='col'>Moisture:</h4>
        <input class="col" title="moisture" name="moisture" id="moisture" />
    </div>
    <div class="row">
        <h4 class='col'>Test Weight:</h4>
        <input class="col" title="test_weight" name="test_weight" id="test_weight" />
    </div>
    <div class="row">
        <h4 class='col'>FM/Dockage:</h4>
        <input class="col" title="fm_dockage" name="fm_dockage" id="fm_dockage" />
    </div>
    <div class="row">
        <h4 class='col'>Plump Thin:</h4>
        <input class="col" title="plump_thin" name="plump_thin" id="plump_thin" />
    </div>
    <div class="row">
        <h4 class='col'>VOM:</h4>
        <input class="col" title="vom" name="vom" id="vom" />
    </div>
    <div class="row">
        <h4 class='col'>COFO:</h4>
        <input class="col" title="cofo" name="cofo" id="cofo" />
    </div>
    <div class="row">
        <h4 class='col'>Other:</h4>
        <input class="col" title="other" name="other" id="other" />
    </div>
    <div class="row">
        <h4 class='col'>Bushels:</h4>
        <input class="col" title="bushels" type="number" id="bushels" name="bushels"/>
    </div>
    <div class="row">
        <h4 class='col'>Dockage Price:</h4>
        <input class="col" title="dockage" type="number" id="dockage" name="dockage"/>
    </div>
    <div class="row">
        <h4 class='col'>Comment:</h4>
        <textarea class="col" title="comment" id="comment" name="comment"/>
    </div>
HTMLBLOCK;

$new_ticket_form_2 = <<<HTMLBLOCK
    <h4><input class="row" title="protein" name="protein" id="protein" /></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
    <h4></h4>
HTMLBLOCK;
$new_product_form = <<<HTMLBLOCK
<form class="form-group text-center" action="" method="post" id="new_product_form">
    <div class="row">
        <h6 class="col">Product Name</h6>
        <input class="col" title="name" name="name" id="Name" />
    </div>
</form>
HTMLBLOCK;

$new_bin_form = <<<HTMLBLOCK
<form class="form-group text-center" action="" method="post" id="new_bin_form">
    <div class="row">
        <h6 class="col">Location</h6>
        <input class="col" title="location" name="location" id="Location" />
    </div>
</form>
HTMLBLOCK;

//New Client Form
$new_client_form = <<<HTMLBLOCK
<form class="form-group text-center" action="" method="post" id="new_client_form">
    <div class="row">
        <h6 class="col">Name</h6>
        <input class="col" title="name" name="name" id="Name" />
    </div>
    <div class="row">
        <h6 class="col">Street</h6>
        <input class="col" title="s_address" name="s_address" id="s_address" />
    </div>
    <div class="row">
        <h6 class="col">City</h6>
        <input class="col" title="city" name="city" id="city" />
    </div>
    <div class="row">
        <h6 class="col">Type</h6>
        <select class="col" title="city" name="type" id="type">
            <option>--Please Select One--</option>
            <option value="seller">Seller</option>
            <option value="buyer">Buyer</option>
            <option value="both">Both</option>
        </select>
    </div>
    <div class="row">
        <h6 class="col">State</h6>
        <select class="col" title="state" name="state" id="state">
                <option value="ND">North Dakota</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
            </select>
    </div>
    <div class="row">
        <h6 class="col">Zip Code</h6>
        <input class="col" title="zip" name="zip" id="zip" />
    </div>
    <div class="row">
        <h6 class="col">Phone Number</h6>
        <input class="col" title="phone" name="phone" id="phone" />
    </div>
</form>
    <div id="contact_container"></div>
    <div id="new_contact_button">
      <button class="btn btn-primary" id="new_contact" type="button">New Contact</button>
    </div>

<div id="err_form"></div>
HTMLBLOCK;

$client_contracts_table_head = <<<HTMLBLOCK
<table id='full_contracts_table' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Contract ID</th>
                <th class='text-center'>Contract N0</th>
                <th class='text-center'>Product</th>
                <th class='text-center'>Contract Quantity</th>
                <th class='text-center'>Contract End Date</th>
                <th class='text-center'>Current Quantity</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$activity_table_head = <<<HTMLBLOCK
<table id='full_activity_table' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Ticket ID</th>
                <th class='text-center'>Date</th>
                <th class='text-center'>Product</th>
                <th class='text-center'>Quantity</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$contact_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Contact Name</th>
                <th class='text-center'>Contact Street Address</th>
                <th class='text-center'>Contact State</th>
                <th class='text-center'>Contact Zip</th>
                <th class='text-center'>Contact phone</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

//Login Form
$login_form = <<<HTMLBLOCK
<div id="err" class="fixed-top"></div>
<form class="form-group" action="" method="post" id="login-form">
    <div class="form-group">
        <label class="col-12 control-label no-padding" for="username">Email</label>

        <div class="col-12 no-padding">
            <input title="username" name="username" id="username" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-12 control-label no-padding" for="password">Password</label>

        <div class="col-12 no-padding">
            <input type="password" title="password" name="password" id="password" />
        </div>
    </div>
    <input onclick="login()" type="submit" value="Login" class="btn btn-primary btn-md">
</form>

HTMLBLOCK;

//Header HTML
$header_html = <<<HTMLBLOCK
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js'></script>
    <script src="/js/handler.js"></script>
    <script src="/js/functions.js"></script>
HTMLBLOCK;

//User Table Header
$user_table_head = <<<HTMLBLOCK
    <table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>User ID</th>
                <th class='text-center'>First Name</th>
                <th class='text-center'>Last Name</th>
                <th class='text-center'>Email</th>
                <th class='text-center'>Level</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

//Client Table Header
$client_table_head = <<<HTMLBLOCK
    <table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Client ID</th>
                <th class='text-center'>Name</th>
                <th class='text-center'>Phone</th>
                <th class='text-center'>Type</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

//Ticket Table Header
$ticket_table_head = <<<HTMLBLOCK
    <table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th>Ticket No</th>
                <th>Date</th>
                <th>Product</th>
                <th>Client</th>
                <th>Price</th>
                <th>Bin</th>
                <th>Net Bushels</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

//Edit Client Header
$client_edit_head = <<<HTMLBLOCK
<div class='row' id='edit_client_form'>
    <div class='col-4'>
        <th><h4 class='col'>Name:</h4></th>
        <th><h4 class='col'>Street Address:</h4></th>
        <th><h4 class='col'>City:</h4></th>
        <th><h4 class='col'>State:</h4></th>
        <th><h4 class='col'>Zip Code:</h4></th>
        <th><h4 class='col'>Phone:</h4></th>
    </div>
    <div class='col'>
HTMLBLOCK;

$product_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Product ID</th>
                <th class='text-center'>Product Name</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$carriers_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Carrier ID</th>
                <th class='text-center'>Name</th>
                <th class='text-center'>Street Address</th>
                <th class='text-center'>City</th>
                <th class='text-center'>State</th>
                <th class='text-center'>Zip</th>
                <th class='text-center'>Phone</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$bins_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Bin ID</th>
                <th class='text-center'>Location</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$sale_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Bill ID</th>
                <th class='text-center'>Date</th>
                <th class='text-center'>PO Number</th>
                <th class='text-center'>Delivery Number</th>
                <th class='text-center'>Net lbs</th>
                <th class='text-center'>Bushels</th>
                <th class='text-center'>Client</th>
                <th class='text-center'>Seal Number</th>
                <th class='text-center'>Carrier</th>
                <th class='text-center'>Trailer Number</th>
                <th class='text-center'>Truck Number</th>
                <th class='text-center'>Product</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$analysis_table_head = <<<HTMLBLOCK
<table id='table_id' class='hover table table-striped table-bordered' width='100%' cellspacing="0">
        <thead>
            <tr>
                <th class='text-center'>Analysis ID</th>
                <th class='text-center'>Bin</th>
                <th class='text-center'>Bill of Lading No</th>
                <th class='text-center'>Product</th>
                <th class='text-center'>Fat (%)</th>
                <th class='text-center'>Fiber (%)</th>
                <th class='text-center'>Ash (%)</th>
                <th class='text-center'>Aflatoxin (ppm)</th>
                <th class='text-center'>Protein (%)</th>
                <th class='text-center'>Moisture (%)</th>
                <th class='text-center'>Test Weight</th>
                <th class='text-center'>Vomitoxin (ppm)</th>
            </tr>
        </thead>
        <tbody>
HTMLBLOCK;

$contract_header = <<<HTMLBLOCK
<html>
<head><title>Contract</title><link rel="stylesheet" href="pdf.css"></head>
<body>


    <header>
        <table>
            <tr>
                <td>
                    <p>Casselton ND 58523</p>
                    <p>P| 701.347.4353</p>
                    <p>F| 701.347.4491</p>
                    <p>casseltonvalleygrain@gmail.com</p>
                </td>
                <td>
                    <p>Beulah ND 58523</p>
                    <p>P| 701.873.4413</p>
                    <p>F| 701.873.2241</p>
                    <p>valleygr@westriv.com</p>
                </td>
                <td>
                    <p>Wheatland ND 58079</p>
                    <p>P| 701.633.6173</p>
                    <p>F| 701.633.6172</p>
                    <p>casseltonvalleygrain@gmail.com</p>
                </td>
                <td>
                    <img src="../../images/valley-grain-milling-logo-350.png">
                    <p style="text-align: center; font-weight: 700;font-size:.7em;">www.valleygrainmilling.com</p>
                </td>
            </tr>
            </table>
            <h1>SALE AND PURCHASE CONTRACT FOR COMMODITIES</h1>
    </header>

HTMLBLOCK;
