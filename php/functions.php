<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/14/17
 * Time: 3:16 PM
 */

session_start();

include 'blocks.php';

require_once 'connect.php';



function table_row_creation ($info) {
    $query = "INSERT INTO " .$info['table']. " (" .$info['column']. ") VALUES (" .$info['change']. ")";
    if($info['test'] === 'simple_test') {
        $Row_num = check_query($info);
        if($Row_num < 1){
            table_query($query);
            exit();
        }
        echo "already created";
        exit();
    }
    table_query($query);
}

function table_row_creation_double ($info) {
    global $db;
    $query = "INSERT INTO " .$info['table']. " (" .$info['column']. ") VALUES (" .$info['change']. ")";
    table_query($query);
    $column = $info['column2'] . ',sale_id';
    $ID = mysqli_insert_id($db);
    $change = $info['change2'] . ", '$ID'";
    $query2 = "INSERT INTO " .$info['table2']. " (" .$column. ") VALUES (" .$change. ")";
    table_query($query2);
}

function table_row_deletion ($info) {
    $query = "DELETE FROM " .$info['table']. " WHERE ID='" .$info['selector']. "'";
    table_query($query);
}

function table_change_check ($info){
    $query = "UPDATE " .$info['table']. " SET " .$info['column']. "='" .$info['change']. "' WHERE ID='" .$info['selector']. "'";
    if ($info['function'] === 'simple_test'){
        $row_num = check_query($info);
        if($row_num < 1) {
            table_query($query);
        } else {
            echo 'invalid';
        }
    } else {
        table_query($query);
    }
}

function generate_form ($info){
    $headings = $info['headings'];
    $inputs = $info['inputs'];
    echo "<form class='row'>";
    echo "<div class='col'>";
    foreach ($headings as $heading){
        echo "<span class='row'>$heading</span>";
    }
    echo "</div>";
    echo "<div class='col'>";
    foreach ($inputs as $input){
        echo "<span class='row'>$input</span>";
    }
    echo "</div>";
    echo "</form>";
}

function login ($user_name,$pass_word){
    global $db;
    $query_login = "SELECT * FROM login WHERE email='$user_name' and password ='$pass_word'";
    $result_login = mysqli_query($db, $query_login);
    $row_login = mysqli_fetch_assoc($result_login);
    $row_num = mysqli_num_rows($result_login);
    if ($row_num == 1) {
        $_SESSION['ID'] = $row_login['ID'];
        $_SESSION['UN'] = $row_login['email'];
        $_SESSION['L'] = $row_login['Level'];
        echo "true";
        return;
    }
    echo 'false';
}

function inline_form () {

}
function retrieve_data ($query,$check){
    global $db;
    $array = array();
    $result = mysqli_query($db,$query);
    if($check === 0){
        $array = mysqli_fetch_assoc($result);
        return $array;
    } else {
        $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $array;
    }
}


//  MASTER FUNCTIONS
function table_query ($query) {
    global $db;
    if (mysqli_query($db,$query)){
        echo "true";
    } else {
        echo $query;
    }
}

function check_query ($info){
    global $db;
    $query_check = "SELECT " .$info['check_column']. " FROM " .$info['table']. " WHERE " .$info['check_column']. "='" .$info['change_check']. "'";
    $result_check = mysqli_query($db,$query_check);
    $row_check = mysqli_num_rows($result_check);
    return $row_check;
}

function simple_check_query ($query){
    global $db;
    $query_check = $query;
    $result_check = mysqli_query($db,$query_check);
    $row_check = mysqli_num_rows($result_check);
    return $row_check;
}

function vertical_table ($query,$heading,$ID,$item){
    global $db;
    $rows = database_array($query);
    echo "<table id='horizontal_table' class='table'>";
    echo "<tbody>";
    echo "<tr class='row'>";
    $i = 0;
    foreach($rows as $key => $section) {
        echo "<tr class='row'>";
        if ($key === 'ID' || $key === 'id'){
        }else {
            echo "<th class='col'>" . $heading[$i] . "</th>";
            if ($heading[$i] === 'Product'){
                $query = "SELECT product_name FROM Products WHERE ID = '".$section."'";
                $data = mysqli_fetch_assoc(mysqli_query($db,$query));
                echo "<td class='col text-center' id=".$item[$i]."_value>".$data['product_name']."</td>";

            }else if ($heading[$i] === 'Client'){
                $query = "SELECT name FROM clients WHERE ID = '".$section."'";
                $data = mysqli_fetch_assoc(mysqli_query($db,$query));
                echo "<td class='col text-center' id=".$item[$i]."_value>".$data['name']."</td>";

            }else if ($heading[$i] === 'Bin'){
                $query = "SELECT location FROM bins WHERE ID = '".$section."'";
                $data = mysqli_fetch_assoc(mysqli_query($db,$query));
                echo "<td class='col text-center' id=".$item[$i]."_value>".$data['location']."</td>";

            }else if ($heading[$i] === 'Carrier'){
                $query = "SELECT name FROM carriers WHERE ID = '".$section."'";
                $data = mysqli_fetch_assoc(mysqli_query($db,$query));
                echo "<td class='col text-center' id=".$item[$i]."_value>".$data['name']."</td>";

            }else if ($heading[$i] === 'Password'){
                $output_password = str_repeat ('&#9830', strlen ('password'));
                echo "<td class='col text-center' id=".$item[$i]."_value>".$output_password."</td>";
            } else {
                echo "<td class='col text-center' id=" . $item[$i] . "_value>$section</td>";
            }
            echo "<td class='col' id='" .$item[$i]. "'><button id='$ID' class='btn btn-sm btn-primary pull-right edit'>edit</button></td>";
        }
        echo '</tr>';
        $i++;
    }
    echo "</tbody>";
    echo "</table>";
}

function horizontal_table ($query) {
    $rows = whole_table_array($query);
    foreach ($rows as $section) {
        echo "<tr id='".$section[0]."'>";
        foreach ($section as $row => $value) {
            if ($query === "SELECT ID,date,product_id,client_id,dockage_price,bin_id,net_bushels FROM tickets") {
                if ($row === 2) {
                    $relation = database_array("SELECT product_name FROM Products WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['product_name'] . "</span></td>";
                } else if ($row === 3) {
                    $relation = database_array("SELECT name FROM clients WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['name'] . "</span></td>";
                } else if ($row === 5) {
                    $relation = database_array("SELECT location FROM bins WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['location'] . "</span></td>";
                } else {
                    echo "<td class='center-block text-center'><span class='center-block'>$value</span></td>";
                }
            } else if ($query === "SELECT ID,f_name,l_name,email,Level FROM login") {
                if ($row === 4) {
                    if ($value === '1') {
                        $adjusted = 'Viewer';
                    } else if ($value === '2'){
                        $adjusted = 'Editor';
                    } else {
                        $adjusted = "Admin";
                    }
                    echo "<td class='center-block text-center'><span class='center-block'>$adjusted</span></td>";
                } else {
                    echo "<td class='center-block text-center'><span class='center-block'>$value</span></td>";
                }
            } else if ($query === "SELECT * FROM clients") {
                if ($row === 1) {
                    echo "<td class='center-block text-center client_name'><span class='center-block'>$value</span></td>";
                } else {
                    echo "<td class='center-block text-center'><span class='center-block'>$value</span></td>";
                }
            } else if ($query === "SELECT id,date,PO,deliver_no,pounds,bushels,client_id,seal_number,carrier_id,trailer,truck,product_id FROM sale") {
                if ($row === 6) {
                    $relation = database_array("SELECT name FROM clients WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['name'] . "</span></td>";
                } else
                    if ($row === 8) {
                        $relation = database_array("SELECT name FROM carriers WHERE id='" . $value . "'");
                        echo "<td class='center-block text-center'><span class='center-block'>" . $relation['name'] . "</span></td>";
                    } else
                        if ($row === 11) {
                            $relation = database_array("SELECT product_name FROM Products WHERE ID='" . $value . "'");
                            echo "<td class='center-block text-center'><span class='center-block'>" . $relation['product_name'] . "</span></td>";
                        } else {
                            echo "<td class='center-block text-center'><span class='center-block'>$value</span></td>";
                        }
            } else if ($query === "SELECT ID,bin_id,sale_id,product_id,fat,fiber,ash,aflatoxin,protein,moisture,test_weight,vomitoxin FROM analysis") {
                if ($row === 3) {
                    $relation = database_array("SELECT product_name FROM Products WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['product_name'] . "</span></td>";
                }else if ($row === 1) {
                    $relation = database_array("SELECT location FROM bins WHERE ID='" . $value . "'");
                    echo "<td class='center-block text-center'><span class='center-block'>" . $relation['location'] . "</span></td>";
                } else {
                    echo "<td class='center-block text-center'><span class='center-block'>$value</span></td>";
                }
            } else {
                echo "<td class='center-block text-center'>$value</td>";
            }
        }
        echo '</tr>';
    }
}

function simple_table ($query) {
    global $db;
    $rows = whole_table_array($query);
    foreach ($rows as $section) {
        echo "<tr class='table_row'>";
        foreach ($section as $row => $value) {
            if ($row === 2){
                $product = mysqli_fetch_assoc(mysqli_query($db,"SELECT product_name FROM Products WHERE ID='$value'"));
                echo "<td class='center-block text-center'>".$product['product_name']."</td>";
            } else if ($row === 4){
                echo "<td class='center-block text-center date'>" . date_conversion($value) . "</td>";
            } else {
                echo "<td class='center-block text-center cell'>" . $value . "</td>";
            }
        }
        echo '</tr>';
    }
}

function database_array ($query){
    global $db;
    $results = mysqli_query($db,$query);
    $row = mysqli_fetch_assoc($results);
    return $row;
}

function whole_table_array ($query){
    global $db;
    $results = mysqli_query($db,$query);
    $row = mysqli_fetch_all($results);
    return $row;
}

function select_creation ($query,$func) {
    $data = whole_table_array($query);
    echo "<select class='col' name='$func' id='change' title='$func'><option>--Please Select--</option>";
    foreach ($data as $row) {
        echo "<option value='".$row[0]."'>$row[1]</option>";
    }
    echo "</select>";
}
function select_creation_new ($query,$func) {
    $data = whole_table_array($query);
    echo "<select class='col' name='$func' id='".$func."' title='$func'><option>--Please Select--</option>";
    foreach ($data as $row) {
        echo "<option value='".$row[0]."'>$row[1]</option>";
    }
    echo "</select>";
}

function pdf_display ($ID){
    $query = "SELECT contract FROM contracts WHERE ID ='$ID'";
    $result = mysqli_query($db,$query);
    while ($row = mysqli_fetch_array($result))
    {
        header("Content-Type: application/pdf");
        echo base64_decode($row['contract']);
    }
}

function date_conversion ($str) {
    $date = strtotime($str);
    $conversion = date('M-d-Y', $date);
    return $conversion;
}

function pdf_show ($ID){
$query = "SELECT contract FROM contracts WHERE ID ='362'";
$result = mysqli_query($db,$query);
  while ($row = mysqli_fetch_array($result))
  {
      header("Content-Type: application/pdf");
      echo base64_decode($row['contract']);
  }
}
