<?php
require '../functions.php';
require '../connect.php';
$info['change'] = $_POST['change'];
$info['selector'] = $_POST['ID'];
$function = $_POST['function'];
$info['table'] = 'contacts';
$info['function'] = '';

if ($function === 'Name'){
  $info['column'] = 'name';
  table_change_check($info);
}

if ($function === 'Street Address'){
  $info['column'] = 's_address';
  table_change_check($info);
}

if ($function === 'City'){
  $info['column'] = 'city';
  table_change_check($info);
}

if ($function === 'State'){
  $info['column'] = 'state';
  table_change_check($info);
}

if ($function === 'ZIPCODE'){
  $info['column'] = 'zip';
  table_change_check($info);
}

if ($function === 'Phone Number'){
  $info['column'] = 'phone';
  table_change_check($info);
}

if ($function === 'set_primary'){
  global $db;
  $ID = $_POST['ID'];
  $primary_id = $_POST['primary_id'];
  echo $ID.",".$primary_id;
  $query = "UPDATE contacts SET status='0' WHERE ID='$primary_id'";
  if(mysqli_query($db, $query)){
    $query = "SELECT client_id FROM contacts WHERE ID='$primary_id'";
    $array = mysqli_fetch_assoc(mysqli_query($db, $query));
    echo $array['client_id'];
    $query = "UPDATE contacts SET status ='1' WHERE ID='$ID'";
    mysqli_query($db,$query);
  }

}
