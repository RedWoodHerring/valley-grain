<?php

require '../functions.php';
echo "<div id='contacts_container'>";
$client_id = $_POST['ID'];
$array = whole_table_array("SELECT * FROM contacts WHERE client_id='$client_id'");
$headings = array('Name','Street Address','City','State','ZIPCODE','Phone Number');
foreach($array as $section){

  $i=0;
  if($section[8] === '1'){
    echo "<div id='primary_contact' class='section'>";
    echo "<h5>Primary Contact</h5>";
    foreach($section as $key => $value){
        if ($key > 1 && $key < 8){
          echo "<div id='".$section[0]."' class='value'>";
          echo "<h7>".$headings[$i]."</h7>";
          echo "<p>".$value."</p>";
          echo "<div class='edit_container'><button class='btn btn-sm btn-info edit'>edit</button></div>";
          echo "</div>";
          $i++;
      }
    }
  } else {
    echo "<div class='section'>";
    echo "<h5>Contact ".($i + 2)."</h5>";
    foreach($section as $key => $value){
      if ($key > 1 && $key < 8){
      echo "<h7>".$headings[$i]."</h7>";
      echo "<p class='value'>".$value."</p>";
      echo "<div class='edit_container'><button class='btn btn-sm btn-info edit'>edit</button></div>";
      $i++;
    }

  }
  echo "<button id='".$section[0]."' class='btn btn-warning make_primary'>Make Primary</button>";

}
echo "</div>";
}
echo "</div>";
