<?php
require '../functions.php';
require '../blocks.php';
$client_id = $_POST['ID'];
?>
<h5>Add Contact</h5>
<form id='new_contact'>
  <div class="form_group">
    <h7>Name</h7>
    <input name="name"/>
  </div>
  <div class="form_group">
    <h7>Street Address</h7>
    <input name="s_address"/>
  </div>
  <div class="form_group">
    <h7>City</h7>
    <input name="city"/>
  </div>
  <div class="form_group">
    <h7>State</h7>
    <select name="state">
      <?=$state_select?>
    </select>
  </div>
  <div class="form_group">
    <h7>ZIPCODE</h7>
    <input type="number" maxlength="5" name="zip"/>
  </div>
  <div class="form_group">
    <h7>Phone Number</h7>
    <input type="tel" name="phone"/>
  </div>
  <div class="form_group">
    <h7>Primary Contact</h7>
    <select name="status">
      <option value="0">NO</option>
      <option value="1">YES</option>
    </select>
  </div>
</form>
<p id="<?=$client_id?>" class="btn btn-primary submit">Submit</p>
