<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/14/17
 * Time: 8:38 AM
 */

require '../functions.php';
require '../blocks.php';

if($_POST['function'] === 'contact_form'){
    echo <<<HTMLBLOCK
    <div class="row">
        <h6 class="col">Name</h6>
        <input class="col" title="cname" name="cname" id="cname" />
    </div>
    <div class="row">
        <h6 class="col">Phone</h6>
        <input class="col" title="cphone" name="cphone" id="cphone" />
    </div>
    <div class="row">
        <h6 class="col">Street</h6>
        <input class="col" title="c_s_address" name="c_s_address" id="c_s_address" />
    </div>
    <div class="row">
        <h6 class="col">City</h6>
        <input class="col" title="c_city" name="c_city" id="c_city" />
    </div>
    <div class="row">
        <h6 class="col">State</h6>
        <select class="col" title="c_state" name="c_state" id="c_city">
HTMLBLOCK;

    echo $state_select;
    echo "</select>";
    echo "</div>";
}