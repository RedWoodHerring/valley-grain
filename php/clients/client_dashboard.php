<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/6/17
 * Time: 11:46 AM
 */

require_once '../connect.php';
include '../functions.php';
include '../blocks.php';

$function = $_POST['function'];

if ($function === 'client_dashboard'){
    $date = date("Y-m-d");
    $ID = $_POST['selection'];
    $query = "SELECT name,street_address,city,state,zip,phone,type FROM clients WHERE ID='$ID'";
    $array = database_array($query);
    $heading = array('Name:','Street Address:','City:','State:','Zip:','Phone');
    echo "<h3 id='status'>".$array['type']."</h3>";
    echo "<div id='client_dashboard'>";
    echo "<div id='column_1' class='col'>";
    echo "<div id='info_container'>";
    echo "<h3>Info:</h3>";
    echo "<div class='col'>";
    $i = 0;
    foreach ($array as $key => $value){
      if($key !== 'type'){
        echo "<div class='row'>";
        echo "<h5>".$heading[$i]."</h5>";
        echo "<h7>$value</h7>";
        echo "</div>";
        $i++;
      }
    }
    echo "</div>";
    echo "</div>";
    echo "<div id='contracts_table_container'>";
    echo "<h3>Contracts</h3>";
    echo "<div id='$ID' class='table_buttons'><button id='active_contracts' class='btn btn-sm btn-info'>Active Contracts</button><button class='btn btn-sm btn-warning' id='completed_contracts'>Completed Contracts</button></div>";
    echo "<div id='contracts_table'></div>";
    echo "</div>";
    echo "</div>";
    echo "<div id='column_2' class='col'>";
    $query = "SELECT name,s_address,city,state,zip,phone FROM contacts WHERE client_id='$ID' AND status='1'";
    $array = database_array($query);
    echo "<div id='contacts_table_container'>";
    if (isset($array['name'])){
      echo "<h3>Contacts</h3>";
      echo "<div class='col'>";
      $heading = array('Name:','Street Address:','City:','State:','Zip:','Phone');
      $i = 0;
      foreach ($array as $value){
          echo "<div class='row'>";
          echo "<h5>".$heading[$i]."</h5>";
          echo "<h7>$value</h7>";
          echo "</div>";
          $i++;
      }
      echo "</div>";
    }
    echo "<div id='contact_buttons'>";
    echo "<p id='$ID' class='btn btn-sm btn-info all_contacts'>View All Contacts</p>";
    echo "<p id='$ID' class='btn btn-sm btn-info create_contact'>Add Contact</p>";
    echo "</div>";
    echo "</div>";
    echo "<div id='activity_table_container'>";
    echo "<h3>Recent Activity</h3>";
    echo "<div id='date_picker' class='input'><input id='from_date' type='date' class='date_picker' value='".date("Y-m-d", strtotime("first day of previous month"))."'><div>to</div><input id='to_date' type='date' class='date_picker' value='".$date."'></div>";
    echo "<div id='activity_table'></div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
}
if ($function === 'contracts_table'){
    $ID = $_POST['selection'];
    $status =$_POST['status'];
    $query = "SELECT ID,contract_no,product_id,qty,shipment_end_date,current_qty FROM contracts WHERE client_id='$ID' AND status='$status'";
    echo $client_contracts_table_head;
    simple_table ($query);
    echo "</tbody>";
    echo "</table>";
}

if ($function === 'activity_table_initial'){
    $ID = $_POST['selection'];
    $query = "SELECT ID,date,product_id,net_bushels FROM tickets WHERE client_id='$ID' AND date BETWEEN '".date("Y-m-d", strtotime("first day of previous month"))."' AND '".date("Y-m-d")."'";
    echo $activity_table_head;
    simple_table ($query);
    echo "</tbody>";
    echo "</table>";
}

if ($function === 'activity_table_refresh'){
    $ID = $_POST['selection'];
    $date_from = $_POST['date_from'];
    $date_to = $_POST['date_to'];
    $query = "SELECT ID,date,product_id,net_bushels FROM tickets WHERE client_id='$ID' AND date BETWEEN '".$date_from."' AND '".$date_to."'";
    echo $activity_table_head;
    simple_table ($query);
    echo "</tbody>";
    echo "</table>";
}
