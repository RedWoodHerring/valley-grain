<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/11/17
 * Time: 9:06 AM
 */

include '../functions.php';

$query = "SELECT id,date,client_id,product_id FROM contracts WHERE status='0'";
echo "<table id='contracts_table' class='table-bordered table'>";
echo <<<HTMLBLOCK
<thead>
    <tr>
        <th>Contract ID</th>
        <th>Date</th>
        <th>Client</th>
        <th>Product</th>
    </tr>
</thead>
HTMLBLOCK;

horizontal_table($query);
echo "</table>";