<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/11/17
 * Time: 9:56 AM
 */

require '../functions.php';
$ID = $_POST['ID'];
$row = database_array("SELECT * FROM contracts WHERE ID='$ID'");
$client = database_array("SELECT * FROM clients WHERE ID='".$row['client_id']."'");
$product = database_array("SELECT * FROM Products WHERE ID='".$row['product_id']."'");
$contract = database_array("SELECT * FROM tickets WHERE contract_id='$ID'");
echo "<h3 id='status'>";
if($row['status'] === '0'){
    echo "Active";
} else {
    echo "Completed";
}
echo "</h3>";
echo "<div id='left_column'>";
echo "<h4 class='center-block'>Contract Data</h4>";
echo "<div id='contract_info'>";
echo "<div class='row'><h7>Seller :</h7><p id='".$client['ID']."' class='client_button'>".$client['name']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Contract No :</h7><p>".$row['contract_no']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Contract Date :</h7><p>".date_conversion($row['date'])."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Product :</h7><p>".$product['product_name']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Vendor No :</h7><p>".$row['vendor_no']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Product :</h7><p>".$product['product_name']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Price :</h7><p>$".$row['price']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Contract Quantity :</h7><div><h8 id='total_qty'>".$row['qty']."</h8><h8> ".$row['qty_type']."</h8></div></div>";
echo "<hr>";
echo "<div class='row'><h7>Quantity Received :</h7><p id='current_qty'>".$row['current_qty']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Load Quantity :</h7><p>".$row['load_qty']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Packing :</h7><p>".$row['packing']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>Delivery :</h7><p>".$row['delivery']."</p></div>";
echo "<hr>";
echo "<div class='row'><h7 class='center-block'>Shipment</h7></div>";
echo "<div class='row'><h7>Start Date :</h7><p>".date_conversion($row['shipment_start_date'])."</p></div>";
echo "<hr>";
echo "<div class='row'><h7>End Date :</h7><p>".date_conversion($row['shipment_end_date'])."</p></div>";
echo "<hr>";
echo "</div>";
echo "<canvas id='pie_chart' height='100em' width='100%'></canvas>";
echo "</div>";
echo "<div id='right_column'>";
echo "<h4 class='center-block'>Activity</h4>";
echo "<table id='tickets_table' class='table-bordered table'>";
echo <<<HTMLBLOCK
<thead>
    <tr>
        <th>Ticket ID</th>
        <th>Date</th>
        <th>Bin</th>
        <th>Qty</th>
    </tr>
</thead>
HTMLBLOCK;
simple_table("SELECT ID,date,bin_id,net_bushels FROM tickets WHERE contract_id='$ID'");
echo "</table>";
echo "</div>";
