<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/31/17
 * Time: 1:46 PM
 */
require_once '../connect.php';
include '../functions.php';
include '../blocks.php';
require '../dompdf/autoload.inc.php';

$info['table'] = 'contracts';
$info['column'] = 'client_id,contract_no,date,vendor_no,product_id,qty,qty_type,price,load_qty,packing,shipment_start_date,shipment_end_date,delivery,terms,status';
$info['change'] = "'".$_POST['Client']."', '".$_POST['contract_no']."', '".$_POST['date']."', '".$_POST['vendor_no']."', '".$_POST['Product']."', '".$_POST['contract_qty']."', '".$_POST['qty_type']."', '".$_POST['price']."', '".$_POST['load_qty']."', '".$_POST['packaging']."', '".$_POST['start_date']."', '".$_POST['end_date']."', '".$_POST['delivery']."', '".$_POST['payment']."', '0'";
$info['function'] = '';
table_row_creation($info);
$ID = mysqli_insert_id($db);
$client = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM clients WHERE ID='".$_POST['Client']."'"));
$contract = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM contracts WHERE ID='$ID'"));
$product = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM Products WHERE ID='".$_POST['Product']."'"));

$div_row = "<div>";
$div_column = "<div>";
$end_div = "</div>";
$number = $client['phone'];
$number = '('.substr($number, 0, 3) .') '.
    substr($number, 3, 3) .'-'.
    substr($number, 6);
$date = date_conversion($contract['date']);

$seller_html = "<div class='column_container'>".
    "<div class='left_column'>".
    "<div class='item'><p class='heading'>SELLER: </p><p class='value'>".$client['name']."</p></div>".
    "<div class='item'><p class='heading'>ADDRESS: </p><p class='value'>".$client['street_address']."</p><br><p id='address_line_2'>".$client['city']." ,".$client['state']." ".$client['zip']."</p></div>".
    "<div class='item'><p class='heading'>PHONE: </p><p class='value'>".$number."</p></div>".
    "<div class='item'><p class='heading'>BUYER: </p><p class='value'>Valley Grain Milling, Inc</p></div>".
    "</div>".
    "<div class='right_column' id='top_right'>".
    "<div class='item'><p class='heading'>CONTRACT NO: </p><p class='value'>".$contract['contract_no']."</p></div>".
    "<div class='item'><p class='heading'>CONTRACT DATE: </p><p class='value'>".date_conversion($contract['date'])."</p></div>".
    "<div class='item'><p class='heading'>VENDOR NO: </p><p class='value'>".$contract['vendor_no']."</p></div>".


    "</div>".
    "<hr id='seller_hr'>".
    "<div id='product_column'>".
    "<div class='item'><p class='heading'>PRODUCT: </p><p class='value'>".$product['product_name']."</p></div>".
    "<div class='item'><p class='heading'>QUANTITY TYPE: </p><p class='value'>".ucwords($_POST['qty_type'])."</p></div>".
    "<div class='item'><p class='heading'>CONTRACT QTY: </p><p class='value'>".$_POST['contract_qty']."</p></div>".
    "<div class='item'><p class='heading'>PRICE: </p><p class='value'>$".$_POST['price']."</p></div>".
    "<div class='item'><p class='heading'>LOAD QTY: </p><p class='value'>".$_POST['load_qty']."</p></div>".
    "<div class='item'><p class='heading'>PACKING: </p><p class='value'>".$_POST['packaging']."</p></div>".
    "<div class='item'><p class='heading'>GRADES TO GOVERN: </p><p class='value'></p></div>".
    "<div class='item'><p class='heading' id='shipment'>SHIPMENT</p>".
    "<p class='heading'>Start Date: </p><p id='start_date' class='value'>".date_conversion($_POST['start_date'])."</p>".
    "<p class='heading'>End Date: </p><p id='end_date' class='value'>".date_conversion($_POST['end_date'])."</p>".
    "</div>".
    "<div class='item'><p class='heading'>DELIVERY: </p><p class='value'>".$_POST['delivery']."</p></div>".
    "<div class='item'><p class='heading'>PAYMENT TERMS: </p><p class='value'>".$_POST['payment']."</p></div>".
    "<div class='item'><p class='heading'>APPLICABLE LAW: </p><p class='value'>THIS CONTRACT SHALL BE GOVERNED BY THE LAWS OF THE STATE OF NORTH DAKOTA</p></div>".
    "</div>".
    "</div>".
    "<hr>".
    "<div id='seller_agreement' style='border:.1em solid #00000'><p class='agreement_header'>Seller Agreement of Guarantee</p><p>This is to ensure the products sold and shipped to VGM,Inc. have been/will be handled in a manner to minimized contamination from biological, chemical, and physical hazards. Any product sold or delivered to VGM, Inc. is Seller guaranteed not to be adulterated, misbranded, or unsafe within the meaning of the Federal Food, Drug and Cosmetic Act or within the rules set forth be the Food & Drug Administration, U.S. Department of Agriculture, and local guidelines.</p></div>".
    "<div style='position:relative;'><div style='position:absolute;left: 20pt;width: 190pt;'>".
    "<h4 class='sign_header' >ACCEPTED BY:</h4>".
    "<hr>".
    "<p class='sub'>Print</p>".
    "<hr><p class='sub'>Signature</p>".
    "<hr>".
    "<p class='sub'>Date</p>".
    "</div>".
    "<div id='right_signature'>".
    "<h4 class='sign_header'>Valley Grain Milling:</h4>".
    "<hr>".
    "<p class='sub'>Print</p>".
    "<hr>".
    "<p class='sub'>Signature</p>".
    "<hr>".
    "<p class='sub'>Date</p>".
    "</div></div>".
    "<div id='agreement' style='border:.1em solid #00000'><p>Subject to the laws of the state of ND. Failure to notify us of any descrepancies within ten days constitutes acceptance of this contract and all of its terms. Failure to return contract to Valley Grain in 10 days officially binds contract between seller and buyer.</p></div>"

    //$end_div.
    //$div_column.
    //$div_row."Valley Grain Milling:".$end_div.
    //$div_row."PRINT:______________________________".$end_div.
    //$div_row."Signature:______________________________".$end_div.
    //$div_row."DATE: ".date('M/d/Y').$end_div.
    //$end_div.
    //$end_div.
    //"</table></table>"
;

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($contract_header.$seller_html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

$output = $dompdf->output();
file_put_contents('pdf_invoice.pdf', $output);
$query_external = "<?php $"."query = \"SELECT contract FROM contracts WHERE ID ='".$ID."'\";";
file_put_contents('pdf_pull.php',$query_external.file_get_contents('php_pull_template.php'));

$result = base64_encode(file_get_contents('pdf_invoice.pdf'));
$query = "UPDATE contracts SET contract='$result' WHERE ID='$ID'";
mysqli_query($db,$query);
unlink('pdf_invoice.pdf');
