<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/31/17
 * Time: 10:02 AM
 */

require_once '../connect.php';
include '../functions.php';
$hr = '<hr>';
echo "<form id='new_contract_form' class='row'>";

echo "<div class='col'>";

echo "<div class='row'>";
echo "<div class='col'><h4>Seller:</h4></div>";
echo "<div id='client_select' class='col'>";
select_creation("SELECT ID,name FROM clients",'Client','clients');
echo "</div>";
echo "<div class='col'><button data-target='#my_modal' data-toggle='modal' id='new_client_button' class='btn btn-primary btn-sm pull-right'>Create New</button></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Contract No:</h4></div>";
echo "<div class='col'><input type='number' name='contract_no' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Contract Date:</h4></div>";
echo "<div class='col'><input type='date' name='date' value='".date('Y-m-d')."' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Vendor No:</h4></div>";
echo "<div class='col'><input type='number' name='vendor_no' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Product:</h4></div>";
echo "<div class='col pull-right'>";
select_creation("SELECT ID,product_name FROM Products",'Product','product');
echo "</div>";
echo "<div class='col'><button id='#new_product_button' class='btn btn-primary btn-sm pull-right'>Create New</button></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Quantity Type:</h4></div>";
echo "<div class='col'><select name='qty_type' class='pull-right'><option>--Please Select One--</option><option value='tons'>Tons</option><option value='bushels'>Bushels</option></select></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Contract Quantity:</h4></div>";
echo "<div class='col'><input type='number' name='contract_qty' value='0' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "</div>";
echo "</div>";
echo "</div>";
echo "<div class='col'>";

echo "<div class='row'>";
echo "<div class='col'><h4>Price:</h4></div>";
echo "<div class='col'><input type='number' min='0.01' step='0.01' max='2500' value='00.00' name='price' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Load Quantity:</h4></div>";
echo "<div class='col'><input type='number' name='load_qty'value='0' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Packaging:</h4></div>";
echo "<div class='col'>";
echo "<select id='packaging_select' name='packaging' class='pull-right'>";
echo "<option>--Please Select One--</option>";
echo "<option value='bulk'>Bulk</option>";
echo "<option value='tote'>Tote</option>";
echo "<option value='other'>Other</option>";
echo "</select>";
echo "</div>";
echo "</div>";

//Other Disapearing Div
echo "<div id='packaging_other' class='row hidden'>";
echo "<div class='col'><h4>Please Specify:</h4></div>";
echo "<input class='col' id='packaging_other' class='pull-right'>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Shipment:</h4></div>";
echo "</div>";

echo "<div class='row'>";
echo "<div class='col'><h5>Start Date:</h5></div>";
echo "<div class='col'><input type='date' name='start_date' value='".date('Y-m-d')."' class='pull-right'></div>";
echo "</div>";

echo "<div class='row'>";
echo "<div class='col'><h5>End Date:</h5></div>";
echo "<div class='col'><input type='date' name='end_date' value='".date('Y-m-d')."' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Delivery:</h4></div>";
echo "<div class='col'>";
echo "<select id='delivery_select' name='delivery' class='pull-right'>";
echo "<option>--Please Select One--</option>";
echo "<option value='seller'>Seller</option>";
echo "<option value='buyer'>Buyer Pickup</option>";
echo "<option value='other'>Other</option>";
echo "</select>";
echo "</div>";
echo "</div>";

//Other Disapearing Div
echo "<div id='delivery_other' class='row hidden'>";
echo "<div class='col'><h4>Please Specify:</h4></div>";
echo "<input class='col' id='delivery_other' class='pull-right'>";
echo "</div>";
echo $hr;

echo "<div class='row'>";
echo "<div class='col'><h4>Payment Terms:</h4></div>";
echo "<div class='col'><input name='payment' class='pull-right'></div>";
echo "</div>";
echo $hr;

echo "</div>";

echo "</div>";
echo "</div>";
echo "</form>";
