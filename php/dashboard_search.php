<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 9/8/17
 * Time: 1:24 PM
 */
?>

  <div id="search_box_container">
    <input id='search'/>
  </div>
  <div id="search_form_container">

    <button class="btn btn-primary" id='search_button'>Search</button>
    <h4>Filter Search</h4>
    <form id="search_form">
      <div class="form_group">
        <input type="radio" name="table" value="tickets" checked/>
        <h5>Tickets</h5>
      </div>
      <div class="form_group">
        <input type="radio" name="table" value="clients"/>
        <h5>Clients</h5>
      </div>
      <div class="form_group">
        <input type="radio" name="table" value="contracts"/>
        <h5>Contracts</h5>
      </div>
      <div class="form_group">
        <input type="radio" name="table" value="sale"/>
        <h5>Bill of Lading</h5>
      </div>
    </form>
  </div>

  <script src='/js/dashboard/main.js'></script>
