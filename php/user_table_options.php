<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 8/8/17
 * Time: 10:28 AM
 */

require '../php/connect.php';

$table = 'login';
$primaryKey = 'ID';
$columns = array(
    array(
        'db'  =>  'ID',
        'dt'    =>  'DT_RowId',
    ),
  array('db'    =>  'email', 'dt'    =>  'username'),
  array('db'    =>  'Level',    'dt'    =>  'level'),
    array('db'  =>  'f_name',   'dt'    =>  'first_name'),
    array('db'  =>  'l_name',   'dt'    =>  'last_name')
);
$sql_details = array(
    'user'  => 'homestead',
    'pass'  => 'secret',
    'db'    => 'homestead',
    'host'  => 'valley-grain.app'
);

require( 'ssp.class.php' );
echo json_encode(
    SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
);