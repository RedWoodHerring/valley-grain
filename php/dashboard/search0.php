<?php

require '../functions.php';
require '../connect.php';

if (isset($_POST['input'])){
  $input = $_POST['input'];
  $table = $_POST['table'];
  $check = array();
  if($table === 'login'){
    $columns = array('email','f_name','l_name');
    $selection = "ID,f_name,l_name";
    $printout = "<p>".$array['l_name']."</p>";
    $query_select = $table;
  }
  if($table === 'clients'){
    $columns = array('city','name','state','zip','type');
    $query_select = $table;
    $selection = "ID,name,type";
    $table_header = array('ID','Client Name','Type');
  }
  if($table === 'tickets'){
    $columns = array('clients.name', 'tickets.date','contracts.date','contracts.status');
    $query_select = $table." JOIN clients ON tickets.client_id = clients.ID JOIN Products ON tickets.product_id = Products.ID";
    $selection = "tickets.ID,clients.name,Products.product_name,tickets.date";
    $table_header = array('ID','Client','Product','Date');
  }
  if($table === 'contracts'){
    $columns = array('clients.name','Products.product_name');
    $query_select = $table." JOIN clients ON contracts.client_id = clients.ID JOIN Products ON contracts.product_id = Products.ID";
    $selection = "contracts.ID,contracts.date,clients.name,Products.product_name";
    $table_header = array('ID','Date','Client','Product');
  }
  if($table === 'sale'){
    $columns = array('clients.name','Products.product_name','sale.date');
    $query_select = $table." JOIN clients ON sale.client_id = clients.ID JOIN Products ON sale.product_id = Products.ID";
    $selection = "sale.ID,sale.date,clients.name,Products.product_name";
    $table_header = array('ID','Date','Client','Product');
  }
  if($table === 'login'){
    $columns = array('f_name','l_name');
  }
  echo "<table id='".$table."' class='table'>";
  echo "<thead>";
  echo "<tr>";
  foreach ($table_header as $header){
    echo "<th>$header</th>";
  }
  echo "</thead>";
  echo "</tr>";
  echo "<tbody>";
  foreach($columns as $column){
    $query = "SELECT $selection FROM $query_select WHERE $column LIKE ?";
    //echo $query;
    if($search = mysqli_prepare($db,$query)){
      mysqli_stmt_bind_param($search, "s", $param_term);
      $param_term = "%".str_replace(' ', '%',$input)."%";
      if (mysqli_stmt_execute($search)){
        $result = mysqli_stmt_get_result($search);
        if (mysqli_num_rows($result) > 0){
          $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
          foreach($array as $section){
            if (in_array($section['ID'], $check)){
            } else {
              echo "<tr id='".$section['ID']."'>";
              foreach($section as $value){

                if($table === 'clients'){
                  $printout = "<td class='client'>$value</td>";
                }
                if($table === 'tickets'){
                  $printout = "<td class='ticket'>$value</td>";
                }
                if($table === 'contracts'){
                  $printout = "<td class='contract'>$value</td>";
                }
                if($table === 'sale'){
                  $printout = "<td class='sale'>$value</td>";
                }
                echo $printout;
              }
              echo "</tr>";
              array_push($check, $section['ID']);
            }
          //if($table === 'clients'){
            //$printout = "<p id='".$section['ID']."' class='client'>".$section['name']." ".$section['type']."</p>";
          //}
          //if($table === 'tickets'){
            //$printout = "<p id='".$section['ID']."' class='ticket'>".$section['date']." ".$section['name']." ".$section['product_name']."</p>";
          //}
          //f($table === 'contracts'){
            //$printout = "<p id='".$section['ID']."' class='contract'>".$section['date']." ".$section['name']." ".$section['product_name']."</p>";
          //}

          }
        }
      }
    }
  }
  echo "<tbody>";
  echo "</table>";
  echo "<script src='/js/dashboard/main.js'></script>";
  echo "<script src='/js/dashboard/table_select.js'></script>";
}
