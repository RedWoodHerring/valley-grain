<?php
session_start();
 if ($_SESSION['ID'] == "") {
     header('Location: ../index.php');
 } else {
     include '../php/var.php';
     $level = $_SESSION['L'];
     echo $header_html;
     $user = $_SESSION['UN'];
     if ($_SESSION['L'] == 3) {
         $level = 'Super Admin';
     } else if ($_SESSION['L'] == 2) {
         $level = 'Publisher';
     } else {
         $level = 'Viewer';
     }
 }
$search = file_get_contents('../php/dashboard_search.php');
?>

    <title>Valley Grain Portal</title>

</head>
<body>
<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby=" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-msg" id="modal_msg"></div>
            <div class="modal-scripts" id="modal_scripts"></div>
            <div class="modal-header justify-center">
                <h5 class="modal-title text-center justify-center" id="modal_title"></h5>

            </div>
            <div class="container modal-body" id="modal_body">
            </div>
            <div class="container modal-footer" id="modal_footer">
            </div>
            <div class="container modal-close justify-center center-block"  id="modal_close">
                <button onclick='erase_modal()' class="btn btn-primary" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">Close</span>
                </button>
            </div>
        </div>
    </div>
</div>
	<div class="container-fluid" id="wrapper">
		<div class="row">
			<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2 bg-faded sidebar-style-1">
				<h1 class="site-title"><a href="index.php"><img src="../images/valley-grain-milling-logo-350.png"> </h1>

				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>

				<ul class="nav nav-pills flex-column sidebar-nav">
					<li id="admin_tools" class="nav-item" href="#admin_sub_menu" data-toggle="collapse" data-target="#admin_sub_menu"><a class="nav-link" href=""><em class="fa fa-tachometer"></em>Admin Tools<span class="sr-only"></span></a></li>
            <li id="admin_sub_menu" class="collapse list-group-item-info">
                <span id="users" class="text-secondary"><a class="nav-link" href="#"><em class="fa fa-fa-user"></em>Users<span class="sr-only"></span></a></span>
                <span id="products" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-cubes"></em>Products</a></span>
                <span id="carriers" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-truck"></em>Carriers</a></span>
                <span id="bins" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-industry"></em>Bins</a></span>
            </li>
					<li id="clients" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-users"></em>Clients</a></li>
          <li id="contracts" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-file-text"></em>Contract</a></li>
          <li id="analysis" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-bar-chart"></em>Product Analysis</a></li>
					<li id="tickets" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-ticket"></em>Tickets</a></li>
          <li id="sale" class="nav-item"><a class="nav-link" href="#"><em class="fa fa-credit-card-alt"></em>Bill of Lading</a></li>
					<li class="nav-item"><a class="nav-link" href="#"><em class="fa fa-clock-o"></em>Time Portal</a></li>
					<li class="nav-item"><a class="nav-link" href="#"><em class="fa fa-paperclip"></em>Reports</a></li>
				</ul>
				<a href="#" class="logout-button"><em class="fa fa-power-off"></em>SIGNOUT</a></nav>

			<main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
				<header class="page-header row justify-center">
					<div class="col-md-6 col-lg-8" >
						<h1 id="header_text" class="float-left text-center text-md-left">Dashboard</h1>
					</div>

					<div class="dropdown user-dropdown col-md-6 col-lg-4 text-center text-md-right"><a class="btn btn-stripped dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

						<div class="username mt-1">
							<h4 class="mb-1"><?php echo $user;?></h4>
							<h6 class="text-muted"><?php echo $level ?></h6>
						</div>
						</a>
						<div class="dropdown-menu dropdown-menu-right logout-button" style="margin-right: 1.5rem;" aria-labelledby="dropdownMenuLink">
						     <a class="dropdown-item"><em class="fa fa-power-off mr-1"></em>SIGNOUT</a></div>
					</div>
					<div class="clear"></div>
				</header>
                <section class="row">
                    <div id="err" class="fixed-top"></div>
					<div class="container">
                        <div>
                            <div id="scripts"></div>
                            <div id="container1">
                                <?= !isset($_SESSION['L']) ? $login_form : "<h2>Welcome ".$_SESSION['f_name']."</h2><div class='row'><button class='btn btn-primary' id='create_ticket'>Create Ticket</button><button class='btn btn-primary' id='create_sale'>Create Bill of Lading</button></div>" ?>
                              <?= isset($_SESSION['L']) ? $search : ""?>
                            </div>
                            <div id="containerdata">
                            </div>
                            <div id="container2">
                                <?= isset($_SESSION['L']) ? "<script src='../js/dashboard/recent.js'></script>" : ""?>
                            </div>
                        </div>
					</div>
				</section>
			</main>
		</div>
	</div>
	  </body>
</html>
